#!/usr/bin/env bash
########################################################################
#     ┏┓┏━┓━━━━━━━━━━━━━
#     ┃┃┃┏┛━━━━━━━━━━━━━
#     ┃┗┛┛━┏┓━┏┓┏━━┓┏━━┓
#     ┃┏┓┃━┃┃━┃┃┃━━┫┃━━┫
#     ┃┃┃┗┓┃┗━┛┃┣━━┃┣━━┃
#     ┗┛┗━┛┗━┓┏┛┗━━┛┗━━┛
#     ━━━━━┏━┛┃━━━━━━━━━
#     ━━━━━┗━━┛━━━━━━━━━
#
# This file is part of the Kyss project (https://gitlab.com/ulukyn/kyss).
# Copyright (c) 2020 Nuno Gonçalves (Ulukyn).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

source kyss/dev/unix/utils.sh

header

action=$1
option=$2

function display_action_help {
	c $Black$OnYellow "Usage: $0 $action <option>"
	c $Yellow "List of actions for $action:"
}

if [[ ! -e config.sh ]]
then
	c $Black$OnRed "Warning!!! Missing config.sh. Please answer the questions to generate them"
	echo ""
	c $Cyan "What the name of your app (default are kyssapp.py)?"
	read appname
	echo "#!/bin/bash" > config.sh
	echo "" >> config.sh
	echo "APPNAME="${appname} >> config.sh
	echo ""
	c $Green "config.sh successfuly generated!"
	exit 0
fi

source config.sh

if [[ $action == "run" ]]
then
	source env/bin/activate
	shift
	python -OO -X pycache_prefix=.cache ${APPNAME}.py $*

elif [[ $action == "build" ]]
then

	if [[ $option == "linux" ]]
	then
		bash kyss/dist/linux/build.sh
	elif [[ $option == "windows" ]]
	then
		bash kyss/dist/windows/build.sh
	elif [[ $option == "macos" ]]
	then
		bash kyss/dist/macos/build.sh
	else
		display_action_help
		c $Cyan " - linux  " $White "Build a package for Linux (.zip)"
		c $Cyan " - windows" $White "Build a package for Windows (.exe)"
		c $Cyan " - macos" $White "Build a package for MAcos (.pkg)"
	fi

elif [[ $action == "env" ]]
then
	bash kyss/dev/unix/create_env.sh
else
	c $Black$OnYellow  "Usage: $0 <action> <option>"
	c $Yellow "List of actions :"
	c $Cyan  " - run  " $White "Start the app (Only works if you have installed venv with '$0 env')"
	c $Cyan  " - env  " $White "Create a python virtualenv and install all required packages"
	c $Cyan  " - build" $White "Build packages for distribution of your app"
	echo  ""
	c $Green  "Use $0 <action> without option to get the list of possibles options for the action"
fi
