### Presentation
Ryztart is a new launcher for the MMORPG Ryzom.\
It also acts as an installer and updater.\
It is designed in a modular way to extend its possibilities.

### The core
Ryztart is coded in python using the Kyss framework (for the moment, Kyss and Ryztart are one, there is no separation yet) which is based on [pywebview](https://pywebview.flowrl.com/).

Kyss offers utility classes to manage the whole application and tools to create packages for Linux, Windows and MacOS.\
For MacOS, Kyss uses py2app.\
For Linux and Windows, Kyss generates itself a package.\
Linux and Windows can be generated from Linux.\
Eventually, the MacOS package will also be generated from Linux.

### Install Ryztart
To install Ryztart, the easiest way is to use the ready-to-install packages available on the https://ryzom.com website.\
It is quite possible to launch Ryztart from a git clone. The following section presents the method.

### Start coding
Setting up a development environment for Ryztart is very simple.\
If you want to develop Ryztart or a module you have to start from the test branch ```git checkout test``` otherwise stay on the main branch.\
Once the code is up to date, install the virtual python environment with the command ```./dev.sh env```.\
Then launch Ryztart with the command ```./dev.sh run```.

### Adding a module
To add a module in the current test version, you just have to create a folder in app/modules.\
The folder must contain at least 2 files:
The python file with the name of the module, for example ```mymodule.py``` and which is the basic continent:
```
From kyssModule import KyssModule

class MyModule(KyssModule):
	self.log("Setup MyModule")
	def setup(self):
		self.log("Setup Mods")
```	

And a ```__init__.py``` file with the following code: 
```From . import mymodule
instance = mymodule.MyModule()
```

