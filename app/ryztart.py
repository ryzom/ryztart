#!/usr/bin/env python
########################################################################
#     ┏┓┏━┓━━━━━━━━━━━━━
#     ┃┃┃┏┛━━━━━━━━━━━━━
#     ┃┗┛┛━┏┓━┏┓┏━━┓┏━━┓
#     ┃┏┓┃━┃┃━┃┃┃━━┫┃━━┫
#     ┃┃┃┗┓┃┗━┛┃┣━━┃┣━━┃
#     ┗┛┗━┛┗━┓┏┛┗━━┛┗━━┛
#     ━━━━━┏━┛┃━━━━━━━━━
#     ━━━━━┗━━┛━━━━━━━━━
#
# This file is part of the Kyss project (https://gitlab.com/ulukyn/kyss).
# Copyright (c) 2020 Nuno Gonçalves (Ulukyn).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import os
import sys
import urllib3
import certifi
import platform
import subprocess
import configparser

http = urllib3.PoolManager(cert_reqs="CERT_REQUIRED",  ca_certs=certifi.where())

local_kyss_config = configparser.ConfigParser()
remote_kyss_config = configparser.ConfigParser()

url = "https://gitlab.com/ulukyn/kyss/-/raw/main/setup.cfg"
try:
	response = http.request("GET", url, preload_content=False)
except:
	print("Error downloading {}".format(url))
	remote_kyss_config.read("kyss.cfg")
else:
	if not response.status == 200:
		print("Error Downloading {}".format(url))
		remote_kyss_config.read("kyss.cfg")
	else:
		data = response.read().decode()
		response.release_conn()
		remote_kyss_config.read_string(data)
remote_version = float(remote_kyss_config["metadata"]["version"])

try:
	local_kyss_config.read("kyss.cfg")
	local_version = float(local_kyss_config["metadata"]["version"])
except:
	local_version = 0

print("Kyss version : {} vs {}".format(local_version, remote_version))
if local_version < remote_version: 
	with open("kyss.cfg", "w") as f:
		f.write(data)
		kyss_config = remote_kyss_config
	if platform.system() == "Darwin":
		subprocess.run(["curl", "https://gitlab.com/ulukyn/kyss/-/archive/main/kyss-main.zip", "--output", "kyss.zip"])
		subprocess.run(["rm", "-rf", "kyss-main"])
		subprocess.run(["unzip", "kyss.zip"])
		os.chdir("kyss-main")
		os.remove("setup.cfg")
		subprocess.run([sys.executable, "setup.py", "install", "--force"])
		os.chdir("..")
		subprocess.run(["rm", "-rf", "kyss-main"])
	else:
		subprocess.run([sys.executable, "-m", "pip", "uninstall", "-y", "kyss"])
		subprocess.run([sys.executable, "-m", "pip", "install", "--upgrade", "https://gitlab.com/ulukyn/kyss/-/archive/main/kyss-main.zip"])
else:
	kyss_config = local_kyss_config

local_config = configparser.ConfigParser()
remote_config = configparser.ConfigParser()

url = "https://gitlab.com/ryzom/ryztart/-/raw/main/setup.cfg"
try:
	response = http.request("GET", url, preload_content=False)
except:
	print("Error downloading A {}".format(url))
	remote_config.read("setup.cfg")
else:
	if not response.status == 200:
		print("Error Downloading {}".format(url))
		remote_config.read("setup.cfg")
	else:
		data = response.read().decode()
		response.release_conn()
		remote_config.read_string(data)

remote_version = float(remote_config["metadata"]["version"])

try:
	local_config.read("setup.cfg")
	local_version = float(local_config["metadata"]["version"])
except:
	local_version = 0

app_name = local_config["metadata"]["name"]
app_name = app_name[0].upper()+app_name[1:]
print(app_name, "version : {} vs {}".format(local_version, remote_version))
if local_version < remote_version: 
	with open("setup.cfg", "w") as f:
		f.write(data)
	config = remote_config
else:
	config = local_config

os.chdir("app")
sys.path.append(os.getcwd())

from kyss import kyss
kysspy = kyss.Kyss(config, kyss_config)
kysspy.start()