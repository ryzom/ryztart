import os
import sys
from kyss.module import KyssModule

class RyzomNews(KyssModule):
	
	def getUsedZones(self):
		return {"main": 100}
	
	def setup(self):
		self.log("Setup News")
		try:
			ryzom_news = self.downloadFile("https://app.ryzom.com/app_releasenotes/index.php?lang="+KyssModule.lang+"&ryztart_version="+KyssModule.VERSION, headers={"user-agent": "Ryztart"}).decode("utf8")
		except:
			ryzom_news = ""
		
		html = "<div style='display: flex;'><div style='flex: 1; padding: 0 0 0 5px;'>"+ryzom_news+"</div></div>"

		self.setZone("main", html)
		KyssModule.setup(self)