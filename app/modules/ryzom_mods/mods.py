import os
import sys
from kyss.module import KyssModule

class RyzomMods(KyssModule):
	
	def getUsedZones(self):
		return {"main": 100}
	
	def call_SwitchBetaTestState(self):
		checked = self.getE("#-beta-state", "checked")
		self.setConfig("mods", "beta_test", "1" if checked else "0")
		self.saveConfig()
		self.app.run("ryzom_installer", "CheckNeedUpdate")
	
	def setup(self):
		self.log("Setup Mods")
		
		values = {}
		values["beta_testing?"] = self.getConfig("mods", "beta_test") == "1"
		
		html = self.getTemplateV2("main", values)
		
		self.setZone("main", html)
		KyssModule.setup(self)