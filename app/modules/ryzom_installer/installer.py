import os
import re
import sys
import json
import stat
import lzma
import time
import psutil
import shutil
import locale
import appdirs
import urllib
import hashlib
import zipfile
import platform
import humanize
import xmltodict
import subprocess
import datetime as dt

from io import BytesIO
from queue import Queue
from os import path as p
from os.path import join as d
from dateutil import parser
from threading import Thread

from kyss.module import KyssModule
from kyss.config import KyssConfig

from .utils import *
from .client_installer import RyzomClientInstaller

class RyzomInstaller(KyssModule):

	running_clients = []

	def _init(self):
		self.state = "waiting"
		self.selected_shard = "atys"
		self.first_login = True
		self.logged = False
		self.servers = None
		self.profiles = None
		self.checkThread = None
		#self.need_setup_main = False
		self.ryzom_roaming_path = appdirs.user_data_dir("Ryzom", "", roaming=True)
		self.ryzom_user_path = appdirs.user_data_dir("Ryzom", "", roaming=False)
		self.config = KyssConfig(d(self.ryzom_roaming_path, "ryzom.ini"))
		self.version = int(self.config.get("latest", "version", "1"))
		if self.version < 2:
			self.config.reset()
			self.config.set("latest", "version", str(INI_LAST_VERSION))
		self.clientInstaller = RyzomClientInstaller()
		self.clientInstaller.setMain(self)
		self.clientInstaller.config = self.config

	def setState(self, state):
		self.log("Set state to {}".format(state))
		self.state = state


	def getUsedZones(self):
		return {"header": 1, "footer": 1, "menu": 100}

	def setWindow(self, window):
		KyssModule.setWindow(self, window)
		self.clientInstaller.setWindow(window)

	def getRunningClients(self):
		clients = []
		processName = getClientName()
		for proc in psutil.process_iter():
			try:
				if processName.lower() in proc.name().lower():
					clients.append((proc.pid, proc.name(), proc.cmdline(),))
			except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
				pass
		return clients

	def showMissingRyzomInstallation(self):
		self.setE("#-infos", "")
		self.setE("#-button", "<div class='h'><img style='margin:0; padding: 0; width:32px' src='file:data/icon_install.png' /><div style='padding: 0 5px'>"+self._("install")+"</div></div>")
		self.setE("#-tooltip", self._("install"))
		self.setupE("#-button", "onclick", "function() { pywebview.api.run('"+self.html_id+"', 'Install'); return false }")
		self.call_Install()

	def start(self):
		self.config.refresh()

		self.selected_profile = ""
		self.setE("#-select-profiles", "")
		self.hideE("#-select-profiles")
		self.hideE("#-userfolder-button")

		cfg_ini = self.getModule("ryzom_config_ini")

		all_profiles = cfg_ini.getProfilesFromIni()
		
		profiles_atys = profiles_yubo = profiles_gingo = profiles_rendor = []
		if "Atys" in all_profiles:
			profiles_atys = self.templatize(all_profiles["Atys"], ("name", "shard", "folder"))
		if "Yubo" in all_profiles:
			profiles_yubo = self.templatize(all_profiles["Yubo"], ("name", "shard", "folder"))
		if "Gingo" in all_profiles:
			profiles_gingo = self.templatize(all_profiles["Gingo"], ("name", "shard", "folder"))
		if "Rendor" in all_profiles:
			profiles_rendor = self.templatize(all_profiles["Rendor"], ("name", "shard", "folder"))
				
		if profiles_atys:
			selected_profile = profiles_atys[0]["_id_"]
		else:
			selected_profile = ""
			
		self.setE("#-select-profiles", self.getTemplateV2("profiles", {
			"profiles_atys": profiles_atys,
			"profiles_yubo": profiles_yubo,
			"profiles_gingo": profiles_gingo,
			"profiles_rendor": profiles_rendor,
			"profiles_yubo?": len(profiles_yubo) > 0,
			"profiles_gingo?": len(profiles_gingo) > 0,
			"profiles_rendor?": len(profiles_rendor) > 0,
			}))
		self.showE("#-select-profiles")

		self.call_SelectProfile(selected_profile)

	def call_Install(self):
		self.getModule("app").call_ShowMainPage("config")
		self.getModule("ryzom_config_ini").preventReloadModule(False)
		self.getModule("ryzom_config_ini").setup("servers")


	def call_SelectShard(self, shard):
		cfg_ini = self.getModule("ryzom_config_ini")
		self.log("Selecting shard {}".format(shard))
		domain = cfg_ini.getDomainName(shard)
		servers = cfg_ini.getServersFromIni()

		if shard in servers and type(servers[shard]) == str:
			server_location = servers[shard]
		else:
			server_location = ""

		if not p.isdir(server_location):
			time.sleep(1)
			self.showMissingRyzomInstallation()
			return

		if domain == "ryzom_live" and self.getConfig("mods", "beta_test") == "1":
			domain = "ryzom_beta"


		self.updateClientsRunning()
		
		if len(self.getRunningClients()) > 0:
			self.setE("#-infos", self._("patch_warning_client_running"))

		if platform.system() == "Darwin":
			self.server_location = server_location+"/Ryzom.app/Contents/Resources"
		else:
			self.server_location = server_location
		self.log("Selecting shard {}".format(shard))
		self.log("Selecting domain {}".format(domain))
		self.selected_shard = shard
		self.selected_domain = domain
		self.log(self.server_location)

		self.clientInstaller.checkNeedUpdate(self.server_location, self.selected_domain)
		self.setState("waiting")

	def call_SelectProfile(self, profile):
		self.log("Select profile {}".format(profile))
		self.selected_shortcut = ""
		self.setE("#-select-shortcuts", "")
		self.hideE("#-select-shortcuts")
		selected_shard = "Atys"
		selected_profile_folder = -1
		cfg_ini = self.getModule("ryzom_config_ini")
		self.selected_profile = profile
		all_profiles = cfg_ini.getProfilesFromIni()
		for shard_name, shard_profiles in all_profiles.items():
			if profile in shard_profiles:
				selected_shard = shard_name
				selected_profile_folder = shard_profiles[profile][1]

		shortcuts = cfg_ini.getShortcutsFromIni(profile, selected_shard, skip_disabled=True)
		
		if profile and shortcuts:
			shortcuts = self.templatize(shortcuts, ("name", "args"))
			if self.config.get(profile, "last_shortcut"):
				self.selected_shortcut = self.config.get(profile, "last_shortcut")
				self.log("Select last shortcut {}".format(self.selected_shortcut))
			elif shortcuts:
				self.selected_shortcut = shortcuts[0]["_id_"]
				self.log("Select shortcut {}".format(self.selected_shortcut))
			
			for i in range(len(shortcuts)):
				if shortcuts[i]["_id_"] == self.selected_shortcut:
					shortcuts[i]["selected"] = "selected=\"selected\""
				else:
					shortcuts[i]["selected"] = ""

			if len(shortcuts) > 0:
				self.setE("#-select-shortcuts", self.getTemplateV2("shortcuts", {"shortcuts": shortcuts}))
				self.showE("#-select-shortcuts")

		if selected_profile_folder != -1:
			self.showE("#-userfolder-button")
		

		self.call_SelectShard(selected_shard)


	def call_OpenUserFolder(self):
		cfg_ini = self.getModule("ryzom_config_ini")
		all_profiles = cfg_ini.getProfilesFromIni()
		selected_profile_folder = -1
		for shard_name, shard_profiles in all_profiles.items():
			if self.selected_profile in shard_profiles:
				selected_profile_folder = shard_profiles[self.selected_profile][1]
		self.app.call_OpenFolder(cfg_ini.posixPath(cfg_ini.getLocationUser())+"/"+selected_profile_folder)

	def call_SelectShortcut(self, shortcut):
		self.selected_shortcut = shortcut


	def call_Setup(self):
		self.setup()

	def call_Update(self, domain=""):
		if self.state == "updating" or self.state == "checking" or self.state == "running":
			return
		if not domain:
			domain = self.selected_domain
		self.setE("#-button", self._("please_wait"))
		self.setState("updating")
		self.clientInstaller.udpateFiles(domain)

	def call_CheckNeedUpdate(self):
		self.clientInstaller.checkNeedUpdate(self.server_location, self.selected_domain, True)

	def call_OpenLoginPopup(self, token):
		self.switchE("#-menu-content")

	def call_LogOut(self, token):
		self.logoutWindow = KyssModule.webview.create_window("Ryzom "+self._("logout"), url="https://me.ryzom.com/api/oauth/logout?access_token="+token)
		try:
			self.logoutWindow.events.loaded += self.onLoadedLogoutWindow
		except:
			self.logoutWindow.loaded += self.onLoadedLogoutWindow

	def call_LogIn(self):
		auth_url = "https://me.ryzom.com/api/oauth/authorize?response_type=code&client_id=ryztart&redirect_uri=https%3A%2F%2Fme.ryzom.com%2Fapi%2Foauth%2Fcallback&scope=ryztart&state=ryztart&lang="+KyssModule.lang
		try:
			self.authWindow = KyssModule.webview.create_window("Ryzom "+self._("login"),  width=520, height=440, icon="data/icon", url=auth_url)
		except:
			self.authWindow = KyssModule.webview.create_window("Ryzom "+self._("login"),  width=520, height=440,  url=auth_url)

		try:
			self.authWindow.events.closed += self.onCloseAuthWindow
			self.authWindow.events.loaded += self.onLoadedAuthWindow
		except:
			self.authWindow.closed += self.onCloseAuthWindow
			self.authWindow.loaded += self.onLoadedAuthWindow

	
	def call_Play(self):
		cfg_ini = self.getModule("ryzom_config_ini")
		
		self.log("Selected [{}] [{}] [{}]".format(self.selected_shard, self.selected_profile, self.selected_shortcut))
		
		all_profiles = cfg_ini.getProfilesFromIni()
		profiles = {}
		for shard_name, shard_profiles in all_profiles.items():
			for profile_name, profile in shard_profiles.items():
				profiles[profile_name] = profile
		shortcuts = cfg_ini.getShortcutsFromIni(self.selected_profile)

		if self.selected_profile in profiles:
			self.config.set(self.selected_profile, "last_shortcut", self.selected_shortcut)
			self.config.save()
			folder = profiles[self.selected_profile][1]
			shard = profiles[self.selected_profile][2]
		else:
			folder = ""
			shard = "Atys"

		ryzom_path = self.config.get(shard, "location")
		if not ryzom_path:
			ryzom_path = self.config.get("Atys", "location")
		if not ryzom_path or not p.isdir(ryzom_path):
			time.sleep(1)
			self.showMissingRyzomInstallation()
			return

		
		if self.selected_shortcut in shortcuts:
			args = shortcuts[self.selected_shortcut][1]
		else:
			args = ""
	
		cfg_ini.updateLangInProfile(shard, KyssModule.lang, folder)
	
		client_name = getClientName()
		client_args = [client_name]
		
		if folder != "":
			client_args.append("--profile")
			client_args.append(folder)

		user_args = args.split(" ")
		client_args += user_args

		self.running_clients = self.getRunningClients()
		for running_client in self.running_clients:
			if user_args[0] != "" and len(running_client[2]) > 3 and running_client[1] == folder and user_args[0] == running_client[2][3]:
				self.setE("#-infos", "<span class='error'>"+self._("account_already_connected")+"</span><br />".format(len(self.running_clients)+1))
				return

		shell = True
		if platform.system() == "Linux":
			# made client exectuable
			client_args[0] = d(ryzom_path, client_args[0])
			st = os.stat(ryzom_path+"/"+client_name)
			os.chmod(ryzom_path+"/"+client_name, st.st_mode | stat.S_IEXEC)
			shell = False
		elif platform.system() == "Darwin":
			st = os.stat(ryzom_path+"/"+client_name)
			os.chmod(ryzom_path+"/"+client_name, st.st_mode | stat.S_IEXEC)
			shell = False

		client_args.append("--nopatch")
		client_args.append("1")

		# Check if require EULA and move it to profile folder
		if p.isfile(d(ryzom_path, "show_eula")):
			shutil.move(d(ryzom_path, "show_eula"), d(self.ryzom_roaming_path, folder, "show_eula"))

		self.log("Run in {}: {}".format(ryzom_path, " ".join(client_args)))

		self.updateClientsRunning()

		proc = subprocess.run(client_args, cwd=ryzom_path, shell=shell, capture_output=True)
		if proc.returncode != 0:
			self.setE("popup", self.getTemplateV2("error_running_ryzom", {"error": proc.stderr.decode()}))
			self.showE("popup")

		self.updateClientsRunning()

	def checkShardStatus(self, status):
		shard_status = status
		while shard_status != "ds_open":
			while shard_status == status:
				shard_status = json.loads(self.downloadFile("https://app.ryzom.com/app_arcc/get_services_status.php?command=status&shard="+self.selected_shard+"&lang="+KyssModule.lang))[0]
				time.sleep(10)
			status = shard_status
			self.call_CheckNeedUpdate()

	def updateClientsRunning(self):
		shard_status = json.loads(self.downloadFile("https://app.ryzom.com/app_arcc/get_services_status.php?command=status&shard="+self.selected_shard+"&lang="+KyssModule.lang))
		if shard_status[0] != "ds_open":
			self.setState("updating")
			self.setE("#-button", self._("please_wait"))
			self.setE("#-infos", "<a style='color: orange' href='https://app.ryzom.com/app_arcc/get_services_status.php?shard="+self.selected_shard+"&lang="+KyssModule.lang+"' target='_blank'>"+shard_status[1]+" "+self.selected_shard+"</a> - "+self._(shard_status[0]))
			if not self.checkThread or not self.checkThread.is_alive():
				self.checkThread = Thread(target=self.checkShardStatus, args=(shard_status[0],))
				self.checkThread.start()
		else:
			self.running_clients = self.getRunningClients()
			if len(self.running_clients) == 0:
				self.setState("waiting")
				self.setE("#-infos", shard_status[1]+" "+shard_status[2]+" - "+self._("ready_to_play"))
			else:
				self.setState("running")
				self.setE("#-infos", shard_status[1]+" "+shard_status[2]+" - "+self._("running_clients", len(self.running_clients))+"<br />")

	def call_Configure(self):
		cfg_ini = self.getModule("ryzom_config_ini")

		self.log("Selected [{}] [{}] [{}]".format(self.selected_shard, self.selected_profile, self.selected_shortcut))

		all_profiles = cfg_ini.getProfilesFromIni()
		profiles = {}
		for shard_name, shard_profiles in all_profiles.items():
			for profile_name, profile in shard_profiles.items():
				profiles[profile_name] = profile
		shortcuts = cfg_ini.getShortcutsFromIni(self.selected_profile)

		if self.selected_profile in profiles:
			self.config.set(self.selected_profile, "last_shortcut", self.selected_shortcut)
			self.config.save()
			folder = profiles[self.selected_profile][1]
			shard = profiles[self.selected_profile][2]
		else:
			folder = ""
			shard = "Atys"

		ryzom_path = self.config.get(shard, "location")
		if not ryzom_path:
			ryzom_path = self.config.get("Atys", "location")
		if not ryzom_path or not p.isdir(ryzom_path):
			time.sleep(1)
			self.showMissingRyzomInstallation()
			return

		client_name = getConfiguratorName()
		client_args = [client_name]
		
		if folder != "":
			client_args.append("--profile")
			client_args.append(folder)
				
		shell = True
		if platform.system() == "Linux":
			# made client exectuable
			client_args[0] = d(ryzom_path, client_args[0])
			st = os.stat(d(ryzom_path, client_name))
			os.chmod(d(ryzom_path, client_name), st.st_mode | stat.S_IEXEC)
			shell = False
		elif platform.system() == "Darwin":
			shell = False

		subprocess.run(client_args, cwd=ryzom_path, shell=shell)

	def onCloseAuthWindow(self):
		rci = self.getModule("ryzom_config_ini")
		rci._init()
		self.setup()
	
	def onLoadedLogoutWindow(self):
		try:
			self.logoutWindow.destroy()
		except:
			pass
		rci = self.getModule("ryzom_config_ini")
		rci._init()
		self.setup()

	def onLoadedAuthWindow(self):
		while self.authWindow:
			try:
				url = self.authWindow.get_current_url()
			except KeyError:
				return
				
			if not url:
				return

			if url[:37] == "https://me.ryzom.com/api/oauth/bypass":
				try:
					self.authWindow.destroy()
					self.setConfig("me", "uid", "skip")
					self.setConfig("me", "token", "skip")
				except:
					pass
				self.setup()
			elif url[:38] == "https://me.ryzom.com/api/oauth/success":
				try:
					token = self.authWindow.get_current_url().split("=")[1]
				except KeyError:
					return
				
				hostname =  urllib.parse.quote_plus(platform.node())
				userInfos = self.downloadFile(f"https://me.ryzom.com/api/oauth/user?access_token={token}&hostname={hostname}")
				try:
					userInfos = json.loads(userInfos)
				except:
					pass
				self.setConfig("me", "uid", userInfos["id"])
				self.setConfig("me", "token", token)
				rci = self.getModule("ryzom_config_ini")
				rci.userInfos = userInfos
				rci.module_prevent_reload = False
				rci.setup()
				try:
					self.authWindow.destroy()
				except:
					pass
			else:
				time.sleep(1)

	def setup(self):
		self.log("Setup Ryzom Installer")
		try:
			humanize.i18n.activate(locale.getdefaultlocale()[0])
		except:
			pass

		token = self.getConfig("me", "token")
		self.userInfos = {"name": "", "result": "ko", "message": "bad json"}
		
		rci = self.getModule("ryzom_config_ini")
		self.userInfos = rci.getUserInfos()

		if self.userInfos["result"] == "ko" and  token != "skip":
			if self.first_login:
				self.first_login = False
				self.call_LogIn()
				return
		else:
			self.first_login = False

		self.running_clients = self.getRunningClients()
		if self.state == "updating" or self.state == "checking":
			self.warning("State are {}. Bypassing...".format(self.state))
			return
		elif len(self.running_clients) and self.state == "running":
			self.warning(f"{self.running_clients = }")
			self.warning("{} client(s) are running. Bypassing...".format(len(self.running_clients)))
			return
		elif self.state == "running":
			self.setState("waiting")


		rci.config.refresh()
		## Load saved servers and profiles from ryzom.ini
		try:
			servers = rci.getServersFromIni()
		except:
			time.sleep(2)
			servers = rci.getServersFromIni()

		subtime = ""
		if self.userInfos["result"] == "ok":
			if self.userInfos["subscription"] and "end" in self.userInfos["subscription"]:
				try:
					subtime = humanize.naturaldelta(parser.parse(self.userInfos["subscription"]["end"]) - dt.datetime.now())
				except:
					subtime = ""
			tmpl = self.getTemplateV2("header", {
				"username": self.userInfos["name"],
				"f2p?": self.userInfos["premium"] == "f2p",
				"premium?": self.userInfos["premium"] == "premium",
				"subtime": subtime,
				"sub?": self.userInfos["subscription"] and self.userInfos["subscription"]["type"] == "sub",
				"nosub?": self.userInfos["subscription"] and self.userInfos["subscription"]["type"] == "nosub",
				"logged?": True,
				"token" : token})
		else:
			tmpl = self.getTemplateV2("header", {
				"notlogged?": True,
			})

		self.setZone("header", tmpl)
		self.showZone("header")

		cfg_ini = self.getModule("ryzom_config_ini")
		shard = "Atys"
		servers = cfg_ini.getServersFromIni()

		if shard in servers and type(servers[shard]) == str:
			server_location = servers[shard]
		else:
			server_location = ""
		
		tmpl = self.getTemplateV2("footer", {"have_ryzom_installed?" : p.isdir(server_location), "button_label": self._("ryzom_client_installer.checking")})
		self.setZone("footer", tmpl)
		self.showZone("footer")

		servers_tmpl = self.getTemplateV2("servers", {"servers": self.templatize(servers)})

		KyssModule.setup(self)
		return self.start()
