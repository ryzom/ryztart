import os
import re
import sys
import json
import stat
import lzma
import time
import psutil
import shutil
import locale
import appdirs
import hashlib
import zipfile
import platform
import humanize
import xmltodict
import subprocess
import datetime as dt

from io import BytesIO
from queue import Queue
from os import path as p
from os.path import join as d
from dateutil import parser
from threading import Thread

from kyss.module import KyssModule
from kyss.config import KyssConfig

from .utils import *

class RyzomClientInstaller(KyssModule):

	def _init(self):
		self.html_id = "ryzom_installer"

		self.download_queue = Queue(0)
		self.uncompress_queueA = Queue(0)
		self.uncompress_queueB = Queue(0)
		self.writedisk_queue = Queue(0)

		self.download_worker = Thread(target=self._downloadFilesFromQueue)
		self.uncompress_workerA = Thread(target=self._uncompressFilesFromQueueA)
		self.uncompress_workerB = Thread(target=self._uncompressFilesFromQueueB)
		self.writedisk_worker = Thread(target=self._writediskFilesFromQueue)

		self.download_worker.setDaemon(True)
		self.uncompress_workerA.setDaemon(True)
		self.uncompress_workerB.setDaemon(True)
		self.writedisk_worker.setDaemon(True)

		self.download_worker.start()
		self.uncompress_workerA.start()
		self.uncompress_workerB.start()
		self.writedisk_worker.start()

		self.ryzom_platforms = {"Linux": "linux", "Darwin" : "osx", "Windows": "win"}

		if platform.system() == "Darwin":
			exedll_name = "exedll_osx.bnp"
		elif platform.machine().endswith("64"):
			exedll_name = "exedll_"+self.ryzom_platforms[platform.system()]+"64.bnp"
		else:
			exedll_name = "exedll_"+self.ryzom_platforms[platform.system()]+"32.bnp"
		
		self.unpackables = ("fonts.bnp", "packedsheets.bnp", "cfg.bnp", exedll_name)
	
	
	def setMain(self, main):
		self.main = main

	def _downloadFilesFromQueue(self):
		while True:
			eid, patch_live_url, filename, patches, last_version, destination, unpack_to = self.download_queue.get()
			self.log("Downloading {}...".format(filename))
			self.setE("#-status", self._("dowloading")+" <span style='color: #ffffff'>{}</span>".format(filename))
			self.setupE("#-status_%d" % eid, "style.backgroundColor", "\"white\"")
			self._downloadAndInstallFile(eid, patch_live_url, filename, patches, last_version, destination, unpack_to)
			self.download_queue.task_done()

	def _uncompressFiles(self, eid, destination, filename, compressed, wanted_sha1, unpack_to, mtime):
		self.log("Uncompressing {}...".format(filename))
		self.setE("#-status", self._("uncompressing")+" <span style='color: #ffffff'>{}</span>...".format(filename))
		self.setupE("#-status_%d" % eid, "style.backgroundColor", "\"yellow\"")
		uncompressed = lzma.decompress(compressed)
		local_sha1 = hashlib.sha1(uncompressed).hexdigest()
		if local_sha1 == wanted_sha1:
			self.log("Same sha1 {}".format(filename))
			self.writedisk_queue.put((eid, destination, filename, uncompressed, unpack_to, mtime))
		else:
			self.error("Bad sha1 {}".format(filename))


	def _uncompressFilesFromQueueA(self):
		while True:
			eid, destination, filename, compressed, wanted_sha1, unpack_to, mtime = self.uncompress_queueA.get()
			self._uncompressFiles(eid, destination, filename, compressed, wanted_sha1, unpack_to, mtime)
			self.uncompress_queueA.task_done()

	def _uncompressFilesFromQueueB(self):
		while True:
			eid, destination, filename, compressed, wanted_sha1, unpack_to, mtime = self.uncompress_queueB.get()
			self._uncompressFiles(eid, destination, filename, compressed, wanted_sha1, unpack_to, mtime)
			self.uncompress_queueB.task_done()

	def _writediskFilesFromQueue(self):
		self.need_finish = 2
		while True:
			eid, destination, filename, uncompressed, unpack_to, mtime = self.writedisk_queue.get()
			self.log("Writing {}".format(filename))
			self.setE("#-status", self._("writing_to_disk")+" <span style='color: #ffffff'>{}</span>...".format(filename))
			self.setupE("#-status_%d" % eid, "style.backgroundColor", "\"lightgreen\"")
			if not unpack_to:
				with open(d(destination, "data", filename), "wb") as f:
					f.write(uncompressed)
				os.utime(d(destination, "data", filename), (mtime, mtime))
			else:
				self.log("Unpack {}".format(filename))
				self.setE("#-status", self._("unpacking")+" <span style='color: #ffffff'>{}</span>...".format(filename))
				with open(d(destination, "unpack", filename), "wb") as f:
					f.write(uncompressed)
				self._unpackFile(destination, filename, unpack_to)
				os.utime(d(destination, "unpack" , filename), (mtime, mtime))
			self.setupE("#-status_%d" % eid, "style.backgroundColor", "\"green\"")
			self.writedisk_queue.task_done()

	def _setReadyToPlay(self, domain):
		self.main.setState("waiting")
		self.setE("#-button", "<div class='h'><img style='margin:0; padding: 0; width: 32px' src='file:data/icons/play.png' /><div style='padding: 5px'>"+self._("play")+"</div></div>")
		self.setE("#-tooltip", self._("start_play_ryzom"))
		self.setupE("#-button", "onclick", "function() { pywebview.api.run('"+self.html_id+"', 'Play'); return false }")
		self.setE("#-infos", "{} - ".format(getShardName(domain))+self._("ryzom_uptodate")+" {}".format(self.patch_version))
		self.main.updateClientsRunning()

	def _getPatchLiveUrl(self, domain):
		if domain == "ryzom_dev":
			return "https://yubo.ryzom.com/patch_live/"
			
		if domain == "ryzom_test":
			return "https://gingo.ryzom.com/patch_live/"
			
		if domain == "ryzom_staging":
			return "https://rendor.ryzom.com/patch_live/"
			
		return "https://dl.ryzom.com/patch_live/"

	def _getPatchVersion(self, domain, destination):
		patch_live_url = self._getPatchLiveUrl(domain)
		url = patch_live_url + "%s.version" % domain
		if not p.isdir(d(destination, "unpack")):
			os.makedirs(d(destination, "unpack"))

		dist_version = self.downloadFile(url)
		if dist_version:
			try:
				return int(dist_version.decode("utf-8").split(" ")[0])
			except:
				self.error("bad remote version")
		return 0
	
	def _getLocalPatchVersion(self, domain, destination):
		if p.isfile(d(destination, "unpack", domain+".version")):
			with open(d(destination, "unpack", domain+".version"), "rb") as f:
				try:
					return int(f.read().decode("utf-8").split(" ")[0])
				except:
					self.error("bad local version")
		return -1

	def _getPatchList(self, domain, destination):
		# Get the patch version
		patch_live_url = self._getPatchLiveUrl(domain)

		self.log("Patch Version : {}".format(self.patch_version))
		self.log("Downloading version xml...")
		xml = self.downloadFile(patch_live_url+"%05d/ryzom_%05d_debug.xml" % (self.patch_version, self.patch_version))
		if not xml:
			return ({}, [])
		self.log("Downloading idx...")
		patch_version = self.downloadFile(patch_live_url+"%05d/ryzom_%05d.idx" % (self.patch_version, self.patch_version))
		if patch_version:
			with open(d(destination, "unpack", "ryzom_%05d.idx" % self.patch_version), "wb") as f:
				f.write(patch_version)

		self.log("Getting bnp versions from xml...")
		
		root = xmltodict.parse(xml)
		total_files = 0
		files = {}
		for x in root["xml"]["_Files"]["_Files"]:
			versions = []
			all_versions = x["_Versions"]
			if type(all_versions) != list:
				all_versions = [all_versions]

			for v in all_versions:

				values = []
				for valname, val in v.items():
					if valname == "_HashKey":
						for hashval in val:
							values.append(int(hashval["@value"]))
					else:
						values.append(int(val["@value"]))
				versions.append(values)

			files[x["_FileName"]["@value"]] = versions
			total_files += 1

		self.log("Getting unpack informations from xml...")
		unpack_files = {} #Get bnp to unpack
		for x in root["xml"]["_Categories"]["_Category"]:
			bnp_name = ""
			all_files = x["_Files"]
			if type(all_files) != list:
				all_files = [all_files]
			for v in all_files:
				bnp_name = v["@value"]

			if "_UnpackTo" in x:
				unpack_files[bnp_name] = x["_UnpackTo"]["@value"] #Unpack .ref files, not the bnps
		self.log("All done!")
		return (files, unpack_files)

	def _checkDownloadedFileContent(self, filename, downloaded):
		self.total_downloaded += downloaded
		percent = (100*self.total_downloaded) / self.total_to_download
		if filename and percent % 10 == 0:
			self.setE("#-status", self._("downloading")+" <span style='color: #ffffff'>{}</span>".format(filename))
		self.setupE("#-file_progress", "style.width", "\"{}%\"".format(round(percent)))

	def _checkDownloadedAllFiles(self, downloaded):
		self.total_downloaded += downloaded
		percent = (100*self.total_downloaded) / self.total_to_download
		self.setupE("#-progress", "style.width", "\"{}%\"".format(round(percent)))

	def _downloadAndInstallFile(self, eid, patch_live_url, filename, patches, last_version, destination, unpack_to=""):
		download = patches == []
		if patches:
			self.total_downloaded = 0
			self.total_to_download = len(patches)
			self.setE("#-status", self._("patching")+" <span style='color: #ffffff'>{}</span>".format(filename))
			for patch in patches:
				url = patch_live_url+"%05d/%s_%05d.patch" % (int(patch[XML_VERSION]), p.splitext(filename)[0], int(patch[XML_VERSION]))
				content = self.downloadFile(url)
				if content == None:
					#TODO: self.setE("#_status_%d" % eid, "<span style='color: red'>&#9632;</span><span class='tooltip'>%s</span>" % filename)
					download = True
					break

				with open(destination+"/unpack/"+filename+".patch", "wb") as f:
					f.write(content)

				if p.isfile(destination+"/unpack/"+filename):
					os.remove(destination+"/unpack/"+filename)

				if platform.system() == "Windows":
					dest = destination.encode("utf-8").decode("cp1252")
				else:
					dest = destination
				
				result = subprocess.run([getPatcherName(dest), "-p", dest+"/unpack/"+filename+".patch", "-s", dest+"/data/"+filename, "-d", dest+"/unpack/"+filename], capture_output=True)
				if result.returncode == 0:
					try:
						os.remove(destination+"/unpack/"+filename+".patch")
						os.remove(destination+"/data/"+filename)
						os.rename(destination+"/unpack/"+filename, destination+"/data/"+filename)
						os.utime(destination+"/data/"+filename, (last_version[XML_FILETIME], last_version[XML_FILETIME]))
					except:
						self.error("Error in patch")
						download = True
						break
					self.log("File patched successfully to version {}{}{}".format(self.color("white"), patch[XML_VERSION], self.color("reset")))
					self._checkDownloadedFileContent("", 1)
				else:
					self.error("Error in patch")
					download = True
					break
			self.setupE("#-status_%d" % eid, "style.backgroundColor", "\"green\"")

		if download:
			url = patch_live_url+"%05d/%s.lzma" % (int(last_version[XML_VERSION]), filename)
			self.total_downloaded = 0
			self.total_to_download = int(last_version[XML_7ZFILESIZE])
			content = self.downloadFile(url, "GET", lambda x: self._checkDownloadedFileContent(filename, x))
			if content == None:
				#TODO: self.setE("#_status_%d" % eid, "<span style='color: red'>&#9632;</span><span class='tooltip'>%s</span>" % filename)
				return

			if self.uncompress_queueA.empty():
				self.uncompress_queueA.put((eid, destination, filename, content, toRyzomPatchHex(last_version), unpack_to, last_version[XML_FILETIME]))
			else:
				self.uncompress_queueB.put((eid, destination, filename, content, toRyzomPatchHex(last_version), unpack_to, last_version[XML_FILETIME]))

	def _unpackFile(self, destination, filename, unpack_to):
		if platform.system() == "Windows":
			dest = destination.encode("utf-8").decode("cp1252")
		else:
			dest = destination

		try:
			if unpack_to:
				if not p.isdir(d(destination, "unpack", "tmp")):
					os.makedirs(d(destination, "unpack", "tmp"))
				result = subprocess.call([getPatcherName(dest), "-u", d(dest, "unpack", filename), "-d", d(dest, "unpack" , "tmp")])
			else:
				result = subprocess.call([getPatcherName(dest), "-u", d(dest, "unpack", filename), "-d", dest+unpack_to[1:]])
		except:
			self.error(sys.exc_info()[0])
			self.error("Can't unpack {} -u {} -d {}".format(getPatcherName(dest), d(dest, "unpack", filename), dest+unpack_to[1:]))
			result = 1

		if result == 0:
			filename_no_ext, filename_ext = p.splitext(filename)
			if filename in self.unpackables:
				for unpacked_file in os.listdir(d(destination, "unpack", "tmp")):
					if not p.isfile(d(destination, "unpack", "tmp", unpacked_file)):
						continue
					unpacked_filename, unpacked_file_ext = p.splitext(unpacked_file)
					if not p.isdir(destination+unpack_to[1:]):
						os.makedirs(destination+unpack_to[1:])
					self.log("Move {} to {}".format(d(destination, "unpack", "tmp", unpacked_file), destination+unpack_to[1:]+unpacked_file))
					try:
						os.replace(d(destination, "unpack", "tmp", unpacked_file), destination+unpack_to[1:]+unpacked_file)
					except:
						self.error(d(destination, "unpack", "tmp" ,unpacked_file))
						pass
					if platform.system() == "Darwin":
						if unpacked_file == "ryzom.zip":
							with zipfile.ZipFile(destination+"/ryzom.zip", "r") as z:
								z.extractall(destination+"/../../..")
					if platform.system() == "Linux" or platform.system() == "Darwin":
						if unpacked_file == getClientName():
							# made client exectuable
							st = os.stat(destination+unpack_to[1:]+unpacked_file)
							os.chmod(destination+unpack_to[1:]+unpacked_file, st.st_mode | stat.S_IEXEC)

		else:
			#TODO: self.setE("#_status_%d" % eid, "<span style='color: red'>&#9632;</span><span class='tooltip'>%s</span>" % filename)
			pass

	def checkNeedUpdate(self, destination, domain, force=False):
		if not (self.download_queue.empty() and self.uncompress_queueA.empty() and self.uncompress_queueB.empty() and self.writedisk_queue.empty()):
			return
		
		if self.main.state == "checking":
			self.error("Allready checking")
			return
		
		self.main.setState("checking")
		self.setE("#-button", self._("please_wait"))
		
		patcher_exe = getPatcherName(destination)
		if not p.isfile(patcher_exe):
			if platform.machine().endswith("64"):
				client_zip = self.downloadFile("https://dl.ryzom.com/client_"+self.ryzom_platforms[platform.system()]+"64.zip")
			else:
				client_zip = self.downloadFile("https://dl.ryzom.com/client_"+self.ryzom_platforms[platform.system()]+"32.zip")
			if client_zip:
				filebytes = BytesIO(client_zip)
				myzipfile = zipfile.ZipFile(filebytes)
				myzipfile.extractall(destination)

		self.log("Cheking update in folder {} for {}".format(destination, domain))
		if not p.isdir(destination):
			os.makedirs(destination)
		
		# Check if need patch or not
		self.patch_version = self._getPatchVersion(domain, destination)
		self.local_version = self._getLocalPatchVersion(domain, destination)
		self.setE("#-version", self._("checking"))
		self.setupE("#-version", "style.color", "'yellow'")

		self.log("Remote patch version: {}, Local patch version: {}".format(self.patch_version, self.local_version))
		if (self.patch_version == self.local_version and not force) or self.main.state == "running":
			self.setupE("#-version", "style.color", "'lightgreen'")
			self.setE("#-version", self._("ryzom_uptodate")+" v{}".format(self.local_version))
			self._setReadyToPlay(domain)
			return


		files, unpack_files = self._getPatchList(domain, destination)

		patch_live_url = self._getPatchLiveUrl(domain)

		self.workers = []
		i = len(files.items())+1
		patches_to_do = []
		
		if platform.system() == "Windows":
			if not p.isfile(d(destination, "platforms", "qwindows.dll")) and p.isfile(d(destination, "qwindows.dll")):
				if not p.isdir(d(destination, "platforms")):
					os.makedirs(d(destination, "platforms"))
				shutil.copyfile(d(destination, "qwindows.dll"), d(destination ,"platforms" ,"qwindows.dll" ))
		
		total_size = 0
		all_files = files.items()
		
		self.setE("#-infos", self.getTemplateV2("ryzom_installer:progression"))
		self.warning(self.getTemplateV2("ryzom_installer:progression"))

		total_files = len(all_files)
		i = 0
		
		# Clean unpack
		self.log("Cleaning unpack...")
		for filename in os.listdir(d(destination, "unpack")):
			if p.isfile(d(destination, "unpack", filename)) and not filename in self.unpackables and filename != "%s.version" % domain and filename != "ryzom_%05d.idx" % self.patch_version:
				try:
					os.remove(d(destination, "unpack", filename))
				except:
					self.warning("Can't delete {} in unpack/".format(filename))
		self.log("done!")

		self.log("Start checking of files...")
		for filename, versions in all_files:
			i += 1
			version = versions[-1]
			total_size += version[2]
			filename_no_ext, filename_ext = p.splitext(filename)
			if (filename[:6] != "exedll" or filename in self.unpackables) and filename_ext != ".ref":
				self.setupE("#-progress-text", "innerText", "\""+self._("checking")+" {}...\"".format(filename))
				self.setupE("#-progress-bar", "style.width", "\"{}%\"".format(round((100 * i) / total_files)))
				if filename in self.unpackables:
					if p.isfile(d(destination, "unpack", filename)):
						os.utime(d(destination, "unpack", filename), (version[XML_FILETIME], version[XML_FILETIME]))
						uncorrect_file = False
					else:
						uncorrect_file = True

					unpack_to = unpack_files[filename]
					# Download lastest sha1
					sha1 = self.downloadFile(patch_live_url+"%05d/%s.sha1" % (self.patch_version, filename_no_ext))
					if sha1:
						for h in sha1.decode("utf-8").split("\n"):
							if not h:
								continue
							sh = h.split(" ")
							if len(sh) == 3:
								remote_sha1 = sh[0]
								unpack_file = sh[2]
								self.log("check... "+os.path.abspath(d(destination+unpack_to[1:], unpack_file)))
								if p.isfile(os.path.abspath(d(destination+unpack_to[1:], unpack_file))):
									with open(os.path.abspath(d(destination+unpack_to[1:], unpack_file)), "rb") as f:
										local_sha1 = hashlib.sha1(f.read()).hexdigest()
										if local_sha1 != remote_sha1:
											uncorrect_file = True
								else:
									self.log("File not found: {}".format(d(destination+unpack_to[1:], unpack_file)))
									uncorrect_file = True
								
					versions = files[filename]
					if uncorrect_file:
						self.warning("{} need patch...".format(filename))
						patches_to_do.append([filename, [], versions[-1], unpack_to])
				else:
					destination_file = d(destination, "data", filename)

					vers = versions[-1]
					remote_sha1 = toRyzomPatchHex(vers)
					unpack_to = ""
					if filename in unpack_files:
						unpack_to = unpack_files[filename]
					
					if not p.isfile(destination_file):
						self.log("File not found : {}".format(destination_file))
						patches_to_do.append([filename, [], versions[-1], unpack_to])
					else:
						stats = os.stat(destination_file)
						if version[1] != stats.st_size or version[XML_FILETIME] != stats.st_mtime: # something wrong with this file (size, mtime or both)
							with open(d(destination, "data" , filename), "rb") as f:
								local_sha1 = hashlib.sha1(f.read()).hexdigest()
							have_version = None
							
							if remote_sha1 == local_sha1: # in this case, mtime are different but it's the same file. So, we update the mtime
								os.utime(d(destination, "data", filename), (version[XML_FILETIME], version[XML_FILETIME]))
								continue
							
							for v in range(len(versions) - 1): # get the version of the file (if possible)
								vers = versions[v]
								remote_sha1 = toRyzomPatchHex(vers)
								if remote_sha1 == local_sha1:
									self.log("Found the version of this file: {}".format(vers[XML_VERSION]))
									have_version = v+1
									break
							
							patches_needed = []

							if have_version:
								size_of_all_patches = 0
								for v in range(len(versions)-have_version):
									patches_needed.append(versions[have_version+v])
									size_of_all_patches += int(versions[have_version+v][XML_PATCHSIZE])
								
								if int(versions[-1][XML_7ZFILESIZE]) < size_of_all_patches: # The sum of all patches are higher than the 7z file. So don't use patchs!
									self.log("Too much patchs, better download full file")
								else:
									self.log("This file need {} patches".format(len(patches_needed)))
							else:
								self.warning("File not found in versions")

							patches_to_do.append([filename, patches_needed, versions[-1], unpack_to])
		
		current_version = 99999
		total_patches = 0
		for ptodo in patches_to_do:
			total_patches += len(ptodo[1])
			if ptodo[2] and ptodo[2][0] < current_version:
				current_version = ptodo[2][0]

		self.patch_destination = destination
		if patches_to_do:
			with open(d(self.patch_destination, "show_eula"), "w") as f:
				f.write("1")
			
			self.patches_to_do = patches_to_do
			self.setE("#-button", self._("update"))
			self.setE("#-tooltip", self._("need_update"))
			self.main.setState("need_update")
			patch_change = "➡️ v{}".format(self.patch_version)
			if current_version < self.patch_version:
				patch_change = "v{} ".format(current_version)+patch_change
			self.setE("#-infos", ("{} "+self._("files_need_update")+". {} "+self._("patches_to_do")).format(len(patches_to_do), total_patches))
			self.setE("#-version", self._("patching")+" {}".format(patch_change))
			self.setupE("#-version", "style.color", "'orange'")
			self.setupE("#-button", "onclick", "function() {{ pywebview.api.run('{}', 'Update', '{}'); return false }}".format(self.html_id, domain))
		else:
			self.setupE("#-version", "style.color", "'lightgreen'")
			self.setE("#-version", self._("ryzom_uptodate")+" v{}".format(self.local_version))
			if self.patch_version != self.local_version:
				with open(d(destination, "unpack", domain+".version"), "wb") as f:
					f.write("{} {}".format(self.patch_version, self.patch_version).encode("utf-8"))
			self._setReadyToPlay(domain)
	
	def udpateFiles(self, domain):
		if not p.isdir(d(self.patch_destination, "data")):
			os.makedirs(d(self.patch_destination, "data"))

		if not p.isdir(d(self.patch_destination, "unpack")):
			os.makedirs(d(self.patch_destination, "unpack"))
		
		self.setE("#-infos", "<div id='"+self.html_id+"-status'></div><div id='"+self.html_id+"-file_progress'></div><div id='"+self.html_id+"-slots'></div>")
		eid = 0
		for filename, patches_needed, version, unpack_to in self.patches_to_do:
			if filename:
				self.addToE("#-slots", "<div id='{}-status_{}' class='{}-bnpslot'><span class='tooltip'>{}</span></div>".format(self.html_id, eid, self.html_id,  filename[:-4]))
				self.download_queue.put((eid, self._getPatchLiveUrl(domain), filename, patches_needed, version, self.patch_destination, unpack_to))
				eid += 1
		self.addToE("#-slots", "<div style='clear: both'></div>")
		self.download_queue.join()
		self.uncompress_queueA.join()
		self.uncompress_queueB.join()
		self.writedisk_queue.join()
		self.log("Update finished !")
		
		with open(self.patch_destination+"/unpack/%s.version" % domain, "w") as f:
			f.write("{} {}".format(self.patch_version, self.patch_version))
		self.setupE("#-version", "style.color", "'lightgreen'")
		self.setE("#-version", self._("ryzom_uptodate")+" v{}".format(self.patch_version))
		self._setReadyToPlay(domain)


