import os
import stat
import platform

INI_LAST_VERSION = 2
XML_VERSION = 0
XML_FILESIZE = 1
XML_7ZFILESIZE = 2
XML_FILETIME = 3
XML_PATCHSIZE = 4

def toRyzomPatchHex(xml_version):
	final = ""
	for i in range(5):
		h = hex(xml_version[5+i]).split("x")[-1].zfill(8)
		final += h[6:8]+h[4:6]+h[2:4]+h[0:2]
	return final

def getClientName():
	if platform.system() == "Darwin":
		return "Ryzom.app/Contents/MacOS/Ryzom"

	if platform.system() == "Linux":
		return "ryzom_client"

	return "ryzom_client_r.exe"

def getConfiguratorName():
	if platform.system() == "Darwin":
		return "Ryzom.app/Contents/MacOS/Ryzom"

	if platform.system() == "Linux":
		return "ryzom_configuration_qt"

	return "ryzom_configuration_qt_r.exe"


def getPatcherName(dest=""):
	if platform.system() == "Windows":
		return os.path.join(dest, "ryzom_client_patcher.exe")
	
	patcher_exe = ""
	if platform.system() == "Darwin":
		patcher_exe =  os.path.join(dest, "RyzomClientPatcher")

	if platform.system() == "Linux":
		patcher_exe = os.path.join(dest, "ryzom_client_patcher")
	
	
	if patcher_exe and os.path.isfile(patcher_exe):
		# made patcher exectuable
		st = os.stat(patcher_exe)
		if not bool(st.st_mode & stat.S_IEXEC):
			os.chmod(patcher_exe, st.st_mode | stat.S_IEXEC)
	
	return patcher_exe

def getShardName(name, default="Atys"):
		domains = {"ryzom_live" : "Atys", "ryzom_dev" : "Yubo", "ryzom_test" : "Gingo", "ryzom_staging" : "Rendor", "ryzom_beta": "Atys <span style='color: orange'>(Beta Test)</span>"}
		if name in domains:
			return domains[name]
		return default