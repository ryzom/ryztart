## AUTHOR:  Nuno Gonçalves (Ulukyn)
## OVERHAUL & DOCUMENTATION: (Revs)

## IMPORT Python module(s)
import os
import sys
import glob
import json
import shutil
import random
import urllib
import appdirs
import humanize
import platform
import traceback

## IMPORT from KYSS
from kyss.module import KyssModule
from kyss.config import KyssConfig

## IMPORT from MODULE(s) FUNCTION(s) & CLASS(es)
from os import path as p

INI_LAST_VERSION = 3

class RyzomConfigIni(KyssModule):
	'''Providing interface and management of:  SHARD(s), PROFILE(s) & SHORTCUT(s)
	Also capable of translating old Ryzom Launcher INI file to current version.
	'''

	def _init(self, quiet=True):
		'''Accepting:
			quiet  = BOOL  ← Suppress exceptions FileNotFoundError & NotADirectoryError if TRUE, default:  True
		As final step:  Will request USER INFO from server, if USER is logged in  →  self.userInfos
		'''
		# Allow suppressing of exceptions:  FileNotFoundError & NotADirectoryError
		self.suppressExeptions = bool(quiet)

		self.servers = None      ## {SHARD_NAME : PATH_TO_SHARD_IN_INI}
		self.profiles = None     ## {SHARD_NAME : {PROFILE_ID : (PROFILE_NAME, PROFILE_DIRECTORY_NAME, SHARD_NAME)}}
		self.shortcuts = None    ## {PROFILE_ID : {SHORTCUT_ID : (SHORTCUT_NAME, ARGS, DISABLED)}}
		self.profileDirs = None  ## NOTE:  NEW!!  {PROFILE_DIRECTORY_NAME : [(SHARD_NAME, PROFILE_ID), ...]}
		self.userInfos = None    ##

		# Client DIR_NAME and names of CFG's & INI's.
		self.ryzom = "Ryzom"
		self.client, self.client_default = "client.cfg", "client_default.cfg"
		self.ryz_ini, self.ryz_install = "ryzom.ini", "ryzom_installer.ini"

		# SECTION delimiter, SECTION parts & and SECTION prefix index of:  INI's
		self.delim = "_"
		self.allChars, self.pKey, self.sKey = "all_chars", "profile", "shortcut"
		self.prefix = {k : f'{k}{self.delim}' for k in [self.pKey, self.sKey]}

		# Keys of:  INI's
		self.keys = {'name' : 'name', 'shard' : 'shard', 'folder' : 'folder', 'args' : 'args', 'disabled' : 'disabled', 'location' : 'location', 'profile' : self.pKey, 'shortcut' : self.sKey}

		# Naming:  DOMAIN(s) & SHARD(s)
		# Todo: Rendor must be enabled only for devs
		self.dAtys, self.dYubo, self.dGingo, self.dRendor = "ryzom_live", "ryzom_dev", "ryzom_test", "ryzom_staging"
		self.sAtys, self.sYubo, self.sGingo, self.sRendor = "Atys", "Yubo", "Gingo", "Rendor"
		self.domains = {self.dAtys : self.sAtys, self.dYubo : self.sYubo, self.dGingo : self.sGingo, self.dRendor : self.sRendor}
		self.shards = {v : k for k,v in self.domains.items()}

		# Constructing:  PATH(s)
		self.ryzom_roaming_path = appdirs.user_data_dir(self.ryzom, "", roaming=True)
		self.ryzom_user_path = appdirs.user_data_dir(self.ryzom, "", roaming=False)
		self.config = KyssConfig(p.join(self.getLocationUser(), self.ryz_ini))

		# Sub-PATH for client_default on Mac.
		self.macPathFix = ["Ryzom.app", "Contents", "Resources"]

		# Updating INI VERSION if needed.
		self.version = int(self.config.get("latest", "version", "1"))
		if self.version < 3:
			self.config.reset()
			self.config.set("latest", "version", str(INI_LAST_VERSION))
			self.config.save()

		# Getting USER INFO from server.
		self._getUserInfos()

	def _getUserInfos(self):
		'''Fetching USER INFO from SERVER, if USER is logged in.
		Returning DICTIONARY with USER INFO if successful, else EMPTY DICTIONARY.
		'''
		self.userInfos = {"name": "", "result": "ko", "message": "bad json"}

		# Fetching ACCESS TOKEN, and downloading USER INFO from SERVER if valid.
		token = self.getConfig("me", "token")
		hostname =  urllib.parse.quote_plus(platform.node())
		if token and token != "skip":
			infos = self.downloadFile(f"https://me.ryzom.com/api/oauth/user?access_token={token}&hostname={hostname}")
			if infos:
				try:
					self.userInfos = json.loads(infos)
				except Exception as e:
					self.error(e)

		# Returning DICTIONARY with USER INFO if successful, else EMPTY DICTIONARY.
		return self.userInfos

	def getUserInfos(self):
		return self.userInfos

	def addDefaultProfile(self, pi, name, shard):
		'''Creating a DEFAULT PROFILE in INI and disk.
			pi    = STRING  ← PROFILE SECTION NAME.
			name  = STRING  ← Display NAME of the PROFILE to be created.
			shard = STRING  ← NAME of the SHARD for which the PROFILE should to be linked to.
		Returning STRING containing the NAME of the PROFILE DIRECTORY used.
		'''
		correct_profile_shard = False
		profiles_path = self.getLocationUser()

		# Checking which PROFILE DIRECTORY to use.
		i = 0
		folder = f"{i}"
		while True:
			if self.k.isdir(profiles_path, folder):
				domain = self.getDomainFromClientCfg(self.getLocationFileUserCfg(folder))
				self.log(f"Folder {folder} contain domain: {domain}")
				if domain:
					if shard == self.getShardName(domain):
						correct_profile_shard = True
						break
			else:
				break
			i += 1
			folder = f"{i}"

		keys = {
			self.keys["name"]   : name,
			self.keys["shard"]  : shard,
			self.keys["folder"] : folder
		}

		# Creating PROFILE ENTRY in INI.
		self.setConfig(pi, keys)
		self.log(f"********** SAVE **********\nPROFILE:  {name} of {folder} at {shard}")

		# Creating PROFILE DIRECTORY and client CFG if doesn't exist.
		if not correct_profile_shard:
			self.createProfileFolder(folder)
			self.updateShardInProfile(shard, folder);

		# Returning PROFILE DIRECTORY NAME used.
		return folder

	def askRemove(self, query, args, action):
		'''Query USER for consent -- YES/NO option.
			query  = STRING  ← The question for the USER to answer, will be passed to KyssModule._() for translation and
			                  formatted with:  item_name
			args   = LIST    ← Two STRING(s):  item_id & item_name
			action = STRING  ← What CALL to direct USER to after they made their choice.
		'''
		# Splitting args into:  item_id & item_name
		item_id, item_name = args

		# Ask USER.
		self.setE("popup", self.getTemplateV2("/ask", {
			"question": self._(query, (f"<span class='bold orange'>{item_name}</span>")),
			"yes": self._('ask_yes'),
			"no": self._('ask_no'),
			"module": self.id,
			"action": action,
			"arg": item_id,
			}))

		# Show popup with query.
		self.showE("popup")

	def createProfileFolder(self, folder):
		'''Creating PROFILE DIRECTORY if doesn't yet exist.
			folder  = STRING  ← NAME of the PROFILE DIRECTORY to create.
		'''
		# Making sure to use DEFAULT PATH as it's not supposed to exist yet.
		d = p.join(self.ryzom_roaming_path, folder)
		if not p.isdir(d):
			os.makedirs(d)

	def doRemove(self, item, confirm):
		'''Perform removal of ITEM from INI if USER complied, then hide popup.
			item     = STRING  ← Representing ITEM to remove from:  INI
			confirm  = BOOL    ← TRUE if to perform removal.
		'''
		if confirm:
			self.config.remove(item)
			self.config.save()
		self.hideE("popup")

	def getDomainFromClientCfg(self, filename, default=""):
		'''Get DOMAIN from USER client CFG, with optional fallback to provided DEFAULT PATH.
			filename  = STRING  ← PATH to USER client CFG.
			default   = STRING  ← Optional, PATH to a fall-back client_default CFG.
		Returning STRING with DOMAIN if found, else None.
		'''
		RyzomCfg = self.getModule("ryzom_cfg")
		cfg = RyzomCfg.getCfg(filename)

		# Cutting off early if EMPTY.
		if cfg.empty():
			return None

		# Attempting to get DOMAIN from 'filename', else from ROOT by provided 'default' else RootConfigFilename.
		try:
			domain = cfg.get("Application")[0]
		except:
			try:
				domain = self.getDomainFromClientCfg(cfg.get("RootConfigFilename")[1:-1] if default == "" else default)
			except:
				domain = None
		return domain  # if domain else "ryzom_default"  ## NOTE:  Uncomment if there's an actual need to return a STRING regardless of case.

	def getDomainName(self, shard, default=None):
		'''Performing lookup of DOMAIN NAME.
		    shard    = STRING  ← NAME of the SHARD for which to find a DOMAIN NAME.
			default  = STRING  ← Fall-back if SHARD is invalid, default:  self.dAtys
		Returning DOMAIN NAME of SHARD, else DEFAULT or fall-back.
		'''
		return self.shards.get(shard, default if default else self.dAtys)

	def getDomainVersionsFromPath(self, path):
		'''NOTE:  FOR FUTURE CONTENT !!
		'''
		if p.exists(p.join(path, self.client_default)) and p.exists(p.join(path, "unpack")):
			versions = []
			for x in glob.glob(p.join(path, "unpack", "ryzom_*.version")):
				with open(x) as f:
					versions.append((p.basename(x).replace(".version", ""), f.read()))
			return versions
		return None

	def getLocation(self, paths):
		'''Detect DIRECTORY.
			paths  = LIST/TUPLE  ← Consisting of STRING(s), or LIST(s)/TUPLE(s) which will be joined using:  os.sep
		Returning first existing PATH to a DIRECTORY.
		NOTE:  If NOT in quiet mode, raising FileNotFoundError and outputting to:  self.error(path, with_tb=False)
		'''
		for path in paths:
			if self.k.isdir(path):
				return path

		## NOTE:  Only when no PATH was returned.
		if not self.suppressExeptions:
			self.error(path)
			raise NotADirectoryError

	def getLocationFile(self, path):
		'''Verify if PATH is a FILE.
			path  = STRING or LIST/TUPLE  ← PATH to FILE -- LIST/TUPLE will be joined using:  os.sep
		Returning PATH if is a FILE, else nothing.
		NOTE:  If NOT in quiet mode, raising FileNotFoundError and outputting to:  self.error(path, with_tb=False)
		'''
		if self.k.isfile(path):
			return path
		elif not self.suppressExeptions:
			self.error(path)
			raise FileNotFoundError

	def getLocationFileRootCfg(self, shard, optRoot=None):
		'''Detecting VALID, existing, DOMAIN client_default CFG FILE.
			shard    = STRING  ← NAME of the SHARD to perform lookup of DOMAIN NAME.
			optRoot  = STRING  ← Optional, PATH to an optional DOMAIN INSTALL ROOT which is to be prioritized over DEFAULT.
		Returning STRING with PATH, if failed to identify and existing CFG FILE -- Returning  DEFAULT PATH
		'''
		path = self.getLocationRoot(optRoot)
		server = self.getDomainName(shard)

		# With OS taken into account, performing lookup of PATH.
		cfgFile = p.join(path, server, self.client_default) if platform.system() != "Darwin" else p.join(path, *self.macPathFix, server, self.client_default)
		path = self.getLocationFile(cfgFile)

		return path if path else cfgFile

	def getLocationFileUserCfg(self, folder):
		'''Detecting VALID, existing, client CFG.
		Returning STRING with PATH, if failed to identify an existing CFG FILE -- Returning:  DEFAULT PATH
		'''
		cfgFile = p.join(self.getLocationUser(), folder, self.client)
		path = self.getLocationFile(cfgFile)

		return path if path else cfgFile

	def getLocationRoot(self, optRoot=None):
		'''Detecting VALID, existing, DOMAIN INSTALL ROOT container location.
			optRoot  = STRING  ← Optional, PATH to an optional DOMAIN INSTALL ROOT which is to be prioritized over DEFAULT.
		Returning STRING with PATH, if failed to identify and existing PATH -- Returning  DEFAULT PATH
		'''
		paths = [self.ryzom_user_path, self.ryzom_roaming_path]

		# Allow prioritizing testing an optional ROOT_INSTALL location.
		if isinstance(optRoot, str):
			paths.insert(0, optRoot)

		return self.getLocation(paths)

	def getLocationUser(self):
		'''Detecting VALID, existing, DIRECTORY for CLIENT USER DATA.
		Returning STRING with PATH, if failed to identify an existing PATH -- Returning:  DEFAULT PATH
		'''
		paths = [self.ryzom_roaming_path, self.ryzom_user_path]
		location = self.getLocation(paths)

		return location if location else self.ryzom_roaming_path

	def getProfilesFromIni(self, force=False):
		'''Generating indexes over:  PROFILE(s) & PROFILE_DIRECTORY_NAME(s)
			force  = BOOL  ← Optional, set TRUE if to force refresh of index - default:  FALSE
		Indexes:
			self.profiles    = {SHARD_NAME : {PROFILE_ID : (PROFILE_NAME, PROFILE_DIRECTORY_NAME, SHARD_NAME)}}
			self.profileDirs = {PROFILE_DIRECTORY_NAME : [(SHARD_NAME, PROFILE_ID), ...]}
		Returning:  self.profiles
		'''

		# If already got content, which hasn't been cleaned out, will simply return to save performance.
		if self.profiles and not force:
			return self.profiles

		# Making sure to work with EMPTY DICTIONARIES and fetching SECTION(s) from:  INI
		self.profiles = {}
		domains = {}
		profiles_path = self.getLocationUser()
		sections = self.config.getSections()

		# Building index of DOMAIN(s) and SHORTCUT(s).
		for section in sections:
			# If prefixed PROFILE, indexing necessary data as:  {SHARD_NAME : {PROFILE_ID : (PROFILE_NAME, PROFILE_DIRECTORY_NAME, SHARD_NAME)}}
			if section.startswith(self.prefix[self.pKey]) or section == self.sAtys.lower():
				profile = dict(self.config.get(section))

				# Evaluating if ENTRY is valid, if not removing and moving on to next:  SECTION
				for key in ["folder", "name"]:
					if not profile.get(self.keys[key]):
						self.config.remove(section)
						self.config.save()
						continue

				# Caching:  PROFILE_DIRECTORY_NAME
				p_folder = profile[self.keys["folder"]]

				# Fetching DOMAIN, lookup if not registered, for:  PROFILE_DIRECTORY_NAME
				domain = domains.get(p_folder, self.getDomainFromClientCfg(p.join(profiles_path, p_folder, self.client)))

				# If DOMAIN lookup failed registering ATYS as DOMAIN, plus updating INI and creating DIRECTORY as needed.
				if not domain:
					shard = profile.get(self.keys["shard"], self.sAtys)
					self.createProfileFolder(p_folder)
					self.updateShardInProfile(shard, p_folder)
					domain = self.getDomainName(shard)
				# Else updating INI as needed, plus lookup of:  SHARD_NAME
				else:
					shard = self.getShardName(domain)
					if profile.get(self.keys["shard"]) != shard:
						self.updateShardInProfile(shard, p_folder);

				# Registering/updating DOMAIN for:  PROFILE_DIRECTORY_NAME
				domains[p_folder] = domain
				self.log(f'Domain found at {p_folder} = {domain}')

				# Making sure to work with a DICTIONARY and registering:  PROFILE
				if not self.profiles.get(shard):
					self.profiles[shard] = {}
				self.profiles[shard][section] = (profile[self.keys["name"]], p_folder, shard)

		# Appending PROFILE(s) from:  USER_INFO
		for shard in self.shards:
			# If USER_INFO contains:  SHARD_NAME & PROFILE_NAME
			have_atys = False
			if self.userInfos.get(shard.lower()) and self.userInfos.get(self.keys["name"]):
				# Constructing PROFILE_ID and setting defaults.
				section = self.delim.join([self.pKey, shard, self.userInfos[self.keys["name"]]])
				add_profile = True
				self.log(f"PROFILES = {self.profiles}")

				# If SHARD_NAME is registered, preventing creation of a new PROFILE if PROFILE_ID is already registered, else making sure SHARD_NAME exist as KEY for creation.
				if self.profiles.get(shard):
					add_profile = not bool(self.profiles[shard].get(section))
				else:
					self.profiles[shard] = {}

				if add_profile:
					self.log(f"ADD PROFILES = {self.profiles}")
					self.log(f"-------------------- ADD PROFILE --------------------\nPROFILE: {section} of {shard}")
					pname = f'{self.userInfos[self.keys["name"]]} ({shard})'
					folder = self.addDefaultProfile(section, self.userInfos[self.keys["name"]] if shard == self.sAtys else pname, shard)
					self.profiles[shard][section] = (pname, folder, shard)
					if shard == self.sAtys:
						have_atys = True
			# Else, if current SHARD_NAME is ATYS and SHARD_NAME is not yet registered in PROFILE(s), appending:  DEFAULT_PROFILE
			if shard == self.sAtys:
				if not have_atys and not shard in self.profiles:
					self.profiles[shard] = {}
					folder = self.addDefaultProfile(self.sAtys.lower(), self.sAtys, shard)
					self.profiles[shard][self.sAtys.lower()] = (self.sAtys, folder, shard)

				if self.userInfos.get("game_access") and len(self.userInfos["game_access"]) > 1:
					profile_name = self.delim.join([self.pKey,  self.delim.join([self.sAtys, "_Shared_"])])
					if not self.profiles[shard].get(profile_name):
						for profile_id, profile in self.profiles[shard].items():
							self.profiles[shard][profile_name] = ("- Shared -", profile[1], shard)
							break

		# Listing PROFILE(s) by SHARD and registering index of which PROFILE(s) belongs to which PROFILE_DIRECTORY_NAME:  {PROFILE_DIRECTORY_NAME : [(SHARD_NAME, PROFILE_ID), ...]}
		self.profileDirs = {}
		for shard, profiles in self.profiles.items():
			self.log(f"Profiles in shard === {shard} ===")
			for profile, pTup in profiles.items():
				self.log(f"\t{pTup[0]}")
				self.profileDirs[pTup[1]] = self.profileDirs.get(pTup[1], [])
				self.profileDirs[pTup[1]].append((shard, profile))

		print("PROFILES", self.profiles)
		return self.profiles

	def getServersFromIni(self):
		'''Create index of:  SHARD/CLIENT INSTALL LOCATION(s)
		Index:
			self.servers = {SHARD_NAME : PATH_TO_SHARD_IN_INI}
		Returning DICTIONARY:  self.servers
		'''
		sections = self.config.getSections()
		self.servers = {shard : self.config.get(shard, self.keys["location"]) for shard in self.shards if shard in sections}
		return self.servers

	def getShardName(self, domain, default=None):
		'''Fetch SHARD NAME for matching provided DOMAIN NAME.
			domain   = STRING    ← DOMAIN NAME for lookup of SHARD NAME.
			default  = ANYTHING  ← Optional, anything (which validates as non-EMPTY/-FALSE) to be returned if NOT successful - default:  self.sAtys
		Returning STRING with SHARD NAME, else provided 'default' or fall-back STRING:  self.sAtys
		'''
		return self.domains.get(domain, default if default else self.sAtys)

	def getShortcutsFromIni(self, filter_profile="", filter_shard="", skip_disabled=False):
		'''Generating indexes over:  SHORTCUT(s) of PROFILE(s)
			filter_profile  = STRING  ← Optional, PROFILE_NAME of which to get SHORTCUT(s) from.
			filter_shard    = STRING  ← Optional, fall-back SHARD_NAME when missing in PROFILE_DATA
			                           NOTE:  Only used when using:  'filter_profile'
			skip_disabled   = BOOL    ← Optional, set TRUE if to skip registering any disabled shortcuts - default:  FALSE
		Index:
			self.shortcuts   = {PROFILE_ID : {SHORTCUT_ID : (SHORTCUT_NAME, ARGS, DISABLED)}}
		Returning:  self.shortcuts  --  Or filtered version.
		'''
		# Making sure to work with EMPTY DICTIONARIES and fetching SECTION(s) from:  INI
		if self.shortcuts is None:
			self.shortcuts = {}
		sections = self.config.getSections()

		# For each SECTION which is a:  SHORTCUT_ID
		for section in sections:
			if section.startswith(self.prefix[self.sKey]):
				# Making sure the PROFILE is registered as KEY to work with.
				profile = self.config.get(section, self.pKey)
				if not self.shortcuts.get(profile):
					self.shortcuts[profile] = {}

				# Caching:  SHORTCUT
				shortcut = self.config.get(section)

				# Skipping SHORTCUT(s) without NAME and disabled SHORTCUT(s).
				if not shortcut.get(self.keys["name"]) or (skip_disabled and shortcut.get(self.keys["disabled"]) == "1"):
					continue

				# Defining EMPTY ARG(s) for SHORTCUT(s) without ARG(s).
				if not shortcut.get(self.keys["args"]):
					shortcut[self.keys["args"]] = ""

				# Registering SHORTCUT(s) as ENABLED if not defined.
				if not shortcut.get(self.keys["disabled"]):
					shortcut[self.keys["disabled"]] = "0"

				# Registering:  SHORTCUT
				self.shortcuts[profile][section] = [shortcut[self.keys["name"]], shortcut[self.keys["args"]], shortcut[self.keys["disabled"]] == "1"]
	
		if filter_profile:
			# If filter_profile got SHORTCUT(s)...
			if self.shortcuts.get(filter_profile):
				# Caching:  SHORTCUT(s)
				shortcuts = self.shortcuts[filter_profile]

				# Filtering out:  ACCOUNT_NAME & PASSWORD
				args = ""
				for name, shortcut in shortcuts.items():
					args = shortcut[1].split(" ")
					if len(args) >= 2:
						args = " ".join(args[:2])
						break

				# Registering an ENABLED ALL_CHARACTERS SHORTCUT with remaining ARG(s), if any.
				if args:
					all_chars_shortcut = self.delim.join([self.sKey]+filter_profile.split(self.delim)[1:]+[self.allChars])
					if not all_chars_shortcut in shortcuts and filter_profile[-8:] != "_Shared_":
						shortcuts[all_chars_shortcut] = ("*", args, False)
			# Else, making sure to assign an EMPTY DICTIONARY.
			else:
				shortcuts = {}

			# Attempting to assign PROFILE SHARD, falling back on:  filter_shard
			profile_infos = self.config.get(filter_profile)
			if profile_infos:
				shard = profile_infos.get(self.keys["shard"], filter_shard)
			else:
				shard = filter_shard

			# If SHARD and ACCESS_DATA is available in:  self.userInfos
			if self.userInfos.get(shard.lower()) and self.userInfos.get("game_access") and self.userInfos.get(self.keys["name"]):
				# Constructing PROFILE_ID.
				profile_name = self.delim.join([shard, self.userInfos[self.keys["name"]]])
				
				# If matching the PROFILE to be filtered out...
				if filter_profile == self.delim.join([self.pKey, profile_name]):
					# Constructing SHORTCUT_ID for ALL_CHARACTERS, and ARGS holding ACCESS_DATA.
					shortcut = self.delim.join([self.sKey, profile_name, self.allChars])
					login, password = (self.userInfos["game_access"][0][token] for token in ["tokenA", "tokenB"])
					args = f"{login} {password} "
					# Potentially appending:  ALL_CHARACTERS
					if not skip_disabled or self.config.get(shortcut, self.keys["disabled"]) != "1":
						shortcuts[shortcut] = ("*", args, self.config.get(shortcut, self.keys["disabled"]) == "1")

					# Potentially appending:  PER_CHARACTER
					userInfos = self.userInfos[shard.lower()]
					if userInfos:
						for i, char in enumerate(userInfos):
							si = self.delim.join([self.sKey, profile_name, str(i)])
							if char and (not skip_disabled or self.config.get(si, self.keys["disabled"]) != "1"):
								shortcuts[si] = (char, f"{args}slot {i}", self.config.get(si, self.keys["disabled"]) == "1")
			return shortcuts
		else:
			for shard in self.shards:
				if self.userInfos.get(shard.lower()) and self.userInfos.get("game_access") and self.userInfos.get(self.keys["name"]):
					# Constructing PROFILE_ID and ARGS holding ACCESS_DATA.
					profile_name = self.delim.join([shard, self.userInfos[self.keys["name"]]])
					full_profile_name = self.delim.join([self.pKey, profile_name])

					login, password = (self.userInfos["game_access"][0][token] for token in ["tokenA", "tokenB"])
					args = f"{login} {password} "

					# Making sure the PROFILE is registered as KEY to work with.
					if not self.shortcuts.get(full_profile_name):
						self.shortcuts[full_profile_name] = {} 

					# Constructing SHORTCUT_ID for ALL_CHARACTERS.
					shortcut = self.delim.join([self.sKey, profile_name, self.allChars])
					if not self.shortcuts[full_profile_name].get(shortcut):
						# Setting SHORTCUT_DATA for write-out to:  INI
						keys = {
							self.keys["name"]     : "*",
							self.keys["profile"]  : full_profile_name,
							self.keys["args"]     : args,
							self.keys["disabled"] : "0"
						}
						self.setConfig(shortcut, keys)

						# Registering SHORTCUT in index.
						self.shortcuts[full_profile_name][shortcut] = ["*", args , False]
					elif self.shortcuts[full_profile_name][shortcut][1] != args:
						self.setConfig(shortcut, {self.keys["args"] : args})
						self.shortcuts[full_profile_name][shortcut][1] = args


					userInfos = self.userInfos[shard.lower()]
					if userInfos:
						for i, char in enumerate(userInfos):
							# Constructing SHORTCUT_ID for CHARACTER_SLOT.
							shortcut = self.delim.join([self.sKey, profile_name, str(i)])
							fullargs = f"{args}slot {i}"
							# If SHORTCUT is already registered...
							if self.shortcuts[full_profile_name].get(shortcut):
								# Removing if CHARACTER has no NAME or updating NAME if has changed.
								if not char:
									self.config.remove(shortcut)
									self.config.save()
								elif self.shortcuts[full_profile_name][shortcut][0] != char:
									# If something was changed or added, saving to:  INI
									self.setConfig(shortcut, {self.keys["name"] : char})
									self.shortcuts[full_profile_name][shortcut][0] = char
								elif self.shortcuts[full_profile_name][shortcut][1] != fullargs:
									self.setConfig(shortcut, {self.keys["args"] : fullargs})
									self.shortcuts[full_profile_name][shortcut][1] = fullargs
							elif char :
								keys = {
									self.keys["name"]     : char,
									self.keys["profile"]  : self.delim.join([self.pKey, profile_name]),
									self.keys["args"]     : fullargs,
									self.keys["disabled"] : "0"
								}
								# If something was changed or added, saving to:  INI
								self.setConfig(shortcut, keys)

								# Registering SHORTCUT in index.
								self.shortcuts[full_profile_name][shortcut] = [char, keys[self.keys["args"]], False]
				
				if shard == "Atys" and  self.userInfos.get("game_access") :
					profile_name = self.delim.join([self.pKey, "Atys__Shared_"])
					if not self.shortcuts.get(profile_name):
						self.shortcuts[profile_name] = {} 
					tokens = self.userInfos["game_access"][1:]
					print(tokens)
					for token in tokens:
						chars = token["chars"]
						args = f"{token['tokenA']} {token['tokenB']} "
						for slot, char in enumerate(chars):
							if char :
								shortcut = self.delim.join([self.sKey, "Atys__Shared_", char])
								fullargs = f"{args}slot {slot}"
								self.shortcuts[profile_name][shortcut] = [char, fullargs, False]
		# Returning created index.
		print(self.shortcuts)
		return self.shortcuts

	def getUsedZones(self):
		return {"main": 500}

	def importProfilesAndShortcuts(self):
		# Check ryzom_installer.ini
		profiles = {}
		profiles_path = self.getLocationUser()
		if p.isfile(p.join(profiles_path, self.ryz_install)):
			self.log(f"{self.ryz_install} found at {profiles_path}")
			config = KyssConfig(p.join(profiles_path, self.ryz_install))
			self.log(f"Config = {config}")
			if config.get("profiles", "size"):
				profiles_size = int(config.get("profiles", "size"))
			else:
				profiles_size = 0
			self.log(f"Found {profiles_size} profiles: ")
			for i in range(profiles_size):
				profile_id = self.delim.join([self.pKey, str(i)])
				profile_folder = config.get(profile_id, "id")
				profile_name = config.get(profile_id, "name")
				profile_args = config.get(profile_id, "arguments")
				new_id = self.delim.join([self.pKey, profile_folder])
				domain = self.getDomainFromClientCfg(self.getLocationFileUserCfg(profile_folder), None)
				if domain == None:
					domain = config.get(profile_id, "server")

				shard = self.getShardName(domain)

				if not new_id in profiles:
					profiles[new_id] = profile_id
					keys = {
						self.keys["name"]   : profile_name,
						self.keys["shard"]  : shard,
						self.keys["folder"] : profile_folder
					}

					# Saving to INI if anything changed, creating DIRECTORY structure and updating SHARD in CFG.
					self.setConfig(new_id, keys)
					self.createProfileFolder(profile_folder)
					self.updateShardInProfile(shard, profile_folder)

				keys = {
					self.keys["name"]     : profile_name,
					self.keys["profile"]  : new_id,
					self.keys["args"]     : profile_args,
					self.keys["disabled"] : "0"
				}

				# Saving to INI if anything changed.
				self.setConfig(self.delim.join([self.sKey, str(i)]), keys)

	def preventReloadModule(self, state=None):
		'''Returning TRUE when there's no need to reload the module.
			Listening to:       self.lang                    ← Inherited from: app/kyssModule.py
			Listening to BOOL:  self.module_prevent_reload
		Returning TRUE if to prevent reload else FALSE to allow.
		'''

		if state != None:
			self.module_prevent_reload = state

		# Ensuring module is reloaded IF language changed.
		try:
			self.langOld
		except:
			self.langOld = False

		# Set TRUE wherever/whenever in the CODE if wishing to PREVENT MODULE RELOAD @ user re-access.
		try:
			self.module_prevent_reload
		except:
			self.module_prevent_reload = False

		# Forcing TRUE at first run, and if self.lang has changed or self.module_prevent_reload is FALSE.
		prevent = self.langOld == self.lang and self.module_prevent_reload

		# Updating old language.
		self.langOld = self.lang

		# Returning evaluation.
		return prevent

	def reloadModule(self, display=""):
		'''Forcibly re-loading the MODULE completely.
			display  = STRING  ← Optional, accepting "profiles" or "servers" to select starting VIEW - default:  SHORTCUT's VIEW
		NOTE:  This FUNCTION is called PER DEFAULT by the Settings Management MODULE if registered in it's queue.
		'''
		self.preventReloadModule = False
		self.log(f"[RELOADING]  Module:  {self.id}")
		self._init(self.suppressExeptions)
		self.setup(display)

	def setConfig(self, item, keysAndVals):
		'''Updating INI if something in the batch caused an alteration.
			item         = STRING      ← Representing the ITEM to be passed to:  self.config.set(item)
			keysAndVals  = DICTIONARY  ← Holding KEY:VALUE pairs to be evaluated and set:  self.config.set(item, key, value)
		Returning BOOL - TRUE if something was written to file.
		'''
		update = False
		for key, value in keysAndVals.items():
			if self.config.get(item, key) != value:
				self.config.set(item, key, value)
				update = True

		if update:
			self.config.save()

		return update

	def setMessage(self, message=False, warning=False):
		'''Setting ID 'ryzom_config_ini-message' → 'content' of:  configuration.html
			message  = STRING  ← Message to be displayed, EMPTY STRING to reset.
			warning  = BOOL    ← Optional, set TRUE to display message with warning-sign.
		'''
		if message:
			self.showE("#-message")
			if warning:
				warning = f"<img src=\"file:data/icons/warning.svg\" class="" />"
				message = "".join([warning, message, warning])
			self.setE("#-message", message)
			r = random.randint(108, 202);
			g = random.randint(108, 202);
			b = random.randint(108, 202);
			self.setupE("#-message", "style.backgroundColor", f"\"rgb({r},{g},{b})\"")
		else:
			self.hideE("#-message")

	def updateLangInProfile(self, shard, lang, folder):
		'''Updating LANGUAGE if has changed.
		'''
		cfg = self.getModule("ryzom_cfg").getCfg(self.getLocationFileUserCfg(folder), self.getLocationFileRootCfg(shard), True)
		old_lang = cfg.get("LanguageCode")
		if old_lang != lang:
			self.log(f"Update language of {shard}, profile {folder}:  {old_lang} → {lang}")
			cfg.set("LanguageCode", lang)
			cfg.save()

	def updatePathInformations(self, shard, selected_path, versions):
		'''Update version STRING of each install in SERVER's VIEW.
			shard          = STRING                      ← Representing SHARD_NAME.
			selected_path  = STRING                      ← Representing PATH to INSTALL LOCATION.
			versions       = TUPLE, or LIST of TUPLE(s)  ← Each TUPLE consisting of:  (DOMAIN_NAME, "InstalledVersion CurrentVersionUpstream")
		'''
		sid = f"#-{shard}-message"
		if versions != None and p.isdir(selected_path):
			# NOTE:  NOT IN USE!!
			#disk_usage = shutil.disk_usage(selected_path)
			#total_space = humanize.naturalsize(disk_usage.total, binary=True)
			#free_space = humanize.naturalsize(disk_usage.free, binary=True)

			if not versions:
				# If no VERSION info, but TARGET PATH contains data- & cfg-DIRECTORIES as well as a client_default CFG, displaying related STRING.
				if p.isdir(p.join(selected_path, "data")) and p.isdir(p.join(selected_path, "cfg")) and p.isfile(p.join(selected_path, self.client_default)):
					## NOTE:  Misspelled path_containts_ryzom_install → path_contains_ryzom_install !
					self.setE(sid, self._("path_containts_ryzom_install"))
				else:
					# Else, assuming a new install is needed and displaying related STRING.
					self.setE(sid, self._("new_installation"))
			elif len(versions) == 1:
				# Extracting version info if packed and displaying related formatted STRING.
				version = versions[0]
				self.setE(sid, self._("path_with_ryzom", (version[0], version[1].split(" ")[0])))
			else:
				# Displaying related formatted STRING.
				for version in versions:
					if self.getShardName(version[0]).lower() == shard:
						self.setE(sid, self._("path_contains_version", (version[0], version[1].split(" ")[0])))
						return
				self.setE(sid,  self._("path_contains_version", (version[0], version[1].split(" ")[0])))
		else:
			# A new install is needed and displaying related STRING.
			self.setE(sid, self._("new_installation"))

	def updateShardInProfile(self, shard, folder):
		'''Updating DOMAIN (SHARD) of PROFILE_DIRECTORY_NAME if it has changed, or is missing.
			shard   = STRING  ← Representing SHARD_NAME of the SHARD the PROFILE_DIRECTORY_NAME belongs to.
			folder  = STRING  ← Representing PROFILE_DIRECTORY_NAME in which the CFG-file to update resides.
		'''
		# Fetching CFG and current DOMAIN, setting NONE if failing.
		cfg = self.getModule("ryzom_cfg").getCfg(self.getLocationFileUserCfg(folder), self.getLocationFileRootCfg(shard))
		try:
			current = cfg.get("Application")[0]
		except:
			current = None

		# Fetching desired DOMAIN, and updating/writing to CFG if it has changed or is missing.
		domain = self.getDomainName(shard)
		if domain != current :
			self.log(f"Update {shard} of profile {folder}:  {str(current)} → {domain}")
			cfg.set("Application", [domain, "./client_ryzom_r.exe", "."])
			cfg.save()



	def call_AddShortcutToDesktop(self, args):
		'''NOTE:  FOR FUTURE CONTENT !!
		'''
		pass

	def call_AskRemoveProfile(self, args):
		'''Ask if USER wish to remove:  PROFILE
		'''
		self.askRemove("ask_remove_profile", args, "RemoveProfile")

	def call_AskRemoveShortcut(self, args):
		'''Ask if USER wish to remove:  SHORTCUT
		'''
		self.askRemove("ask_remove_shortcut", args, "RemoveShortcut")

	def call_Close(self):
		'''Closing popup.
		'''
		self.hideE("popup")

	def call_DisplayConfigProfiles(self, force=False):
		'''Constructing PROFILE's VIEW.
			force  = BOOL  ← Optional, set TRUE if to force use of refreshed index - default:  FALSE
		'''
		# Shared between several FUNCTION(s), but not intended for reuse in any other CLASS.
		self.selected_profiles = {}

		# Updating index of PROFILE(s) and SHORTCUT(s) and caching PROFILE(s) base PATH.
		self.getProfilesFromIni(force)
		self.getShortcutsFromIni()
		profiles_path = self.getLocationUser()

		# Summarizing PROFILE(s) as a DICTIONARY ITEM each in a list.
		i = 0
		profiles_html = ""
		if self.profiles:
			for shard, shard_profiles in self.profiles.items():
				for profile_id, profile in shard_profiles.items():
					# Caching PROFILE DATA in SHARED INDEX and assigning LOCAL VARIABLE for DIRECTORY to improve readability.
					self.selected_profiles[profile_id] = {
						"__id__" : profile_id,
						"name"   : profile[0],
						"shard"  : shard,
						"folder" : self.esc(profile[1])
					}
					profile_folder = self.selected_profiles[profile_id]["folder"]

					# If PROFILE_DIRECTORY exist, appending LINK as POSIX along with PROFILE_DIRECTORY_NAME and get DOMAIN_NAME from:  CFG
					if self.k.isdir(profiles_path, profile_folder):
						self.selected_profiles[profile_id]["folder_link?"] = {
							"folder" : self.posixPath(p.join(profiles_path, profile_folder)),
							"name"   : profile_folder
						}
						self.selected_profiles[profile_id]["domain"] = self.getDomainFromClientCfg(self.getLocationFileUserCfg(profile_folder))
					# Else, appending LINK PROFILE_DIRECTORY_NAME and default DOMAIN_NAME.
					else:
						self.selected_profiles[profile_id]["folder_break?"] = {"name" : profile_folder}
						self.selected_profiles[profile_id]["domain"] = self.dAtys

					# Counting, and appending count of, SHORTCUT(s) and preventing removal of PROFILE if got any.
					total_shortcuts = len(self.shortcuts.get(profile_id, []))
					self.selected_profiles[profile_id]["deletable?"] = total_shortcuts == 0
					self.selected_profiles[profile_id]["total_shortcuts"] = total_shortcuts

					# Determining ROW colour-scheme.
					self.selected_profiles[profile_id]["cycle"] = "odd" if i % 2 else "even"
					i += 1

					# Ensuring SHARD_NAME is accurate.
					self.selected_profiles[profile_id]["shard"] = self.getShardName(self.selected_profiles[profile_id]["domain"])

					# Appending formatted ROW TEMPLATE.
					profiles_html += self.getTemplateV2("profile", self.selected_profiles[profile_id])

		# Creating CONTENT from fused ROW TEMPLATE(s), using it to format VIEW and resetting displayed message.
		content = self.getTemplateV2("found_profiles", {
				"profiles_location" : self.posixPath(profiles_path),
				"profiles"          : profiles_html,
				"profiles_infos"    : self._("profiles_infos").replace("\n", "<br />")})
		self.setZone("main", self.getTemplateV2("configuration", {
				"selected_profiles" : "selected",
				"content" : content}))
		self.setMessage()

	def call_DisplayConfigServers(self):
		'''Constructing SERVER's VIEW.
		'''
		# Preparing indexes and caching DEFAULT ROOT PATH.
		self.getProfilesFromIni()
		self.getServersFromIni()
		default_path = self.getLocationRoot()

		# Checking if USER is a Ryzom Team Member.
		ryzom_team = False
		for shard in (self.sYubo, self.sGingo):
			if self.userInfos.get(shard.lower(), None) != None or shard in self.profiles:
				ryzom_team = True
				break

		# Formatting SERVER TEMPLATE based on if Ryzom Team Member and apply as CONTENT of SERVER TEMPLATE.
		content = self.getTemplateV2("found_servers", {"ryzom_team?": ryzom_team})
		self.setZone("main", self.getTemplateV2("configuration", {"selected_servers": "selected", "title": self._("manage_your_ryzom_install"), "content": content}))

		# Depending on if SERVER(s) are detected, attempting to fetch them from the old INI of ryzom_installer and displaying message accordingly.
		ryzom_intallation_path = ""
		if not self.servers:
			if self.k.isfile(default_path, self.ryz_install):
				self.log(f"{self.ryz_install} found at {default_path}")
				config = KyssConfig(p.join(default_path, self.ryz_install))
				ryzom_intallation_path = config.get("common", "installation_directory")
				if ryzom_intallation_path:
					ryzom_intallation_path = ryzom_intallation_path.replace("\\", os.sep).replace("/", os.sep)

			if ryzom_intallation_path:
				self.setMessage(f'{self._("installtion_found_in", (self.esc(ryzom_intallation_path)))}<br />')
			else:
				self.setMessage(f'{self._("choose_ryzom_install_dir")}<br />')
		else:
			self.setMessage(f'{self._("changing_ryzom_install")}<br />')

		# If no install PATH assigned, assigning DEFAULT.
		if not ryzom_intallation_path:
			ryzom_intallation_path = default_path

		for shard in self.shards:
			# Resolving path to each DOMAIN  --  If not registered, saving to:  INI
			if self.servers and self.servers.get(shard):
				path = self.servers[shard]
			else:
				path = p.join(ryzom_intallation_path, self.getDomainName(shard))
				self.setConfig(shard, {self.keys["location"] : path})
			if not self.k.isdir(path):
				path = p.join(default_path, self.getDomainName(shard))
			self.log(f"PATH {shard}:  {path}")

			# Fetching VERSION for each DOMAIN and updating VIEW.
			versions = self.getDomainVersionsFromPath(path)
			self.setupE(f"#-{shard.lower()}-path", "value", f"\"{self.esc(path)}\"")
			self.updatePathInformations(shard.lower(), path, versions)

	def call_DisplayConfigShortcuts(self):
		'''Constructing SHORTCUT's VIEW.
		'''
		# Shared between several FUNCTION(s), but not intended for reuse in any other CLASS.
		self.selected_shortcuts = {}

		# Preparing indexes and caching DEFAULT PROFILE PATH.
		self.getProfilesFromIni()
		self.getShortcutsFromIni()
		profiles_path = self.getLocationUser()

		iA = iB = 0
		shortcuts_htmlA = shortcuts_htmlB = ""
		for profile, profile_shortcuts in self.shortcuts.items():
			for shortcut_id, shortcut in profile_shortcuts.items():
				# Caching SHORTCUT DATA in SHARED INDEX and assigning LOCAL VARIABLE for ARGS.
				self.selected_shortcuts[shortcut_id] = {
					"__id__"   : shortcut_id,
					"name"     : shortcut[0],
					"profile"  : profile,
					"args"     : self.esc(shortcut[1]),
					"disabled" : shortcut[2]
				}
				args = self.selected_shortcuts[shortcut_id]["args"]

				# Recreating ARGS for display, censoring up PASSWORD.
				arg_i = 0
				profile_args = []
				a = [a for a in args.split(" ")]
				censor = True if len(a) and len(a[0]) and a[0][0] not in ["/", "-"] else False
				for arg in a:
					profile_args.append("*****" if censor and arg_i == 1 else arg)
					arg_i += 1
				self.selected_shortcuts[shortcut_id]["args"] = " ".join(profile_args)

				# Resolving PROFILE:  SHARD_NAME, PROFILE_DIRECTORY_NAME & PROFILE_NAME  --  Breaking when found, skipping SHORTCUT if not found.
				profile_shard = ""
				for shard, shard_profiles in self.profiles.items():
					if shard_profiles.get(profile):
						profile_folder = shard_profiles[profile][1]
						profile_shard = shard
						profile_name = shard_profiles[profile][0]
						break
				if not profile_shard:
					continue
				self.selected_shortcuts[shortcut_id]["shard"] = profile_shard


				# If PROFILE_DIRECTORY exist, appending LINK as POSIX along with PROFILE_DIRECTORY_NAME and get DOMAIN_NAME from:  CFG
				if self.k.isdir(profiles_path, profile_folder):
					self.selected_shortcuts[shortcut_id]["folder_link?"] = {
						"folder" : self.posixPath(p.join(profiles_path, profile_folder)),
						"name"   : profile_name}
					self.selected_shortcuts[shortcut_id]["domain"] = self.getDomainFromClientCfg(self.getLocationFileUserCfg(profile_folder))
				# Else, appending to SHARED INDEX for LINK PROFILE_DIRECTORY_NAME and default DOMAIN_NAME.
				else:
					self.selected_shortcuts[shortcut_id]["folder_break?"] = {"name": profile_name}

				# Determining ROW colour-scheme and if DISABLED/ENABLED/REMOVABLE.
				self.selected_shortcuts[shortcut_id]["remove?"] = shortcut_id[0] != "_"
				if self.selected_shortcuts[shortcut_id]["disabled"]:
					self.selected_shortcuts[shortcut_id]["cycle"] = "odd" if iB % 2 else "even"
					self.selected_shortcuts[shortcut_id]["enable?"] = {"id": shortcut_id}
					self.selected_shortcuts[shortcut_id]["disabled"] = "disabled"
					iB += 1
					shortcuts_htmlB += self.getTemplateV2("shortcut", self.selected_shortcuts[shortcut_id])
				else:
					self.selected_shortcuts[shortcut_id]["cycle"] = "odd" if iA % 2 else "even"
					self.selected_shortcuts[shortcut_id]["disable?"] = {"id": shortcut_id}
					iA += 1
					shortcuts_htmlA += self.getTemplateV2("shortcut", self.selected_shortcuts[shortcut_id])

				# Restoring ARGS to uncensored format.
				self.selected_shortcuts[shortcut_id]["args"] = args

		# Creating CONTENT from fused ROW TEMPLATE(s), using it to format VIEW and resetting displayed message.
		content = self.getTemplateV2("found_shortcuts", {
			"shortcuts": f"{shortcuts_htmlA}{shortcuts_htmlB}",
			"shortcuts_infos" : self._("shortcuts_infos").replace("\n", "<br />")})
		self.setZone("main", self.getTemplateV2("configuration", {
			"selected_shortcuts": "selected",
			"content" : content}))
		self.setMessage()

	def call_EditProfile(self, profile_id="", external=False):
		'''Creating EDIT PROFILE DIALOGUE POPUP.
			profile_id  = STRING  ← Optional, representing the PROFILE to be edited - default NEW PROFILE:  EMPTY STRING
			external    = BOOL    ← Optional, set TRUE to return DICTIONARY with PROFILE_DATA - default:  FALSE
		'''
		# Resetting message.
		self.setMessage()

		# Fetching PROFILE_DATA if possible, else creating TEMPLATE DATA.
		if profile_id:
			profile = self.selected_profiles[profile_id]
		else:
			# Selecting first available numeric DIRECTORY as TEMPLATE VALUE for new PROFILE and marking as new.
			i = 0
			folder = f"{i}"
			profiles_path = self.getLocationUser()
			while self.k.isdir(profiles_path, folder):
				i += 1
				folder = f"{i}"
			profile = {"name": "", "folder": folder}
			profile["new?"] = True

		# Setting ID & SHARD(s) and creating DIALOGUE POPUP.
		profile["__id__"] = profile_id
		profile["shards"] = self.templatize({k : k for k in self.shards})

		# If NOT called from external module, display EDIT VIEW - else returning:  PROFILE_DATA
		if not external:
			self.setE("popup", self.getTemplateV2("edit_profile", profile))
			self.showE("popup")
		else:
			return profile

	def call_EditShortcut(self, shortcut_id="", external=False):
		'''Creating EDIT SHORTCUT DIALOGUE POPUP.
			shortcut_id  = STRING  ← Optional, representing the SHORTCUT to be edited - default NEW SHORTCUT:  EMPTY STRING
			external    = BOOL    ← Optional, set TRUE to return DICTIONARY with SHORTCUT_DATA - default:  FALSE
		'''
		# Resetting message.
		self.setMessage()

		# If a SHORTCUT_ID was defined, fetching related information else preparing for a new SHORTCUT.
		if shortcut_id and self.selected_shortcuts.get(shortcut_id):
			shortcut = self.selected_shortcuts[shortcut_id]
			shortcut["Profile"] = f'{shortcut["profile"][0].upper()}{shortcut["profile"][1:]}'
			args = shortcut["args"].split(" ", 3)
			name = ""
			password = ""

			# Skipping account and slot info collection if first ARG is a command flag/switch.
			if args[0] and args[0][0] not in ["/", "-"]:
				name = args[0]
				if len(args) > 1 and args[1]:
					if args[1][0] != "/":
						password = args[1]
						if len(args) > 3:
							shortcut[f"slot{args[3]}?"] = True

			# Display SHORTCUT SELECTION header and set parameters.
			shortcut["new?"] = True
			shortcut["argname"] = name
			shortcut["argpassword"] = password
			shortcut["__id__"] = shortcut_id
		else:
			# Display SHORTCUT SELECTION header and set parameters.
			shortcut = {
				"profile" : "",
				"shard"   : "",
				"new?"    : True
			}

		# Collecting PROFILE_DATA related to SHORTCUT.
		shortcut["profiles"] = []
		for shard, profiles_list in self.getProfilesFromIni().items():
			if not shortcut["shard"] or shard == shortcut["shard"]:
				for pid, infos in profiles_list.items():
					if infos[0] and not "@" in infos[0]:
						shortcut["profiles"].append({
							"id"       : pid,
							"name"     : f"{shard} / {infos[0]}",
							"selected" : "selected='selected'" if pid == shortcut["profile"] else ""
						})

		# If NOT called from external module, display EDIT VIEW - else returning:  SHORTCUT_DATA
		if not external:
			self.setE("popup", self.getTemplateV2("edit_shortcut", shortcut))
			self.showE("popup")
		else:
			return shortcut

	def call_EnableDisableShortcut(self, shortcut_id, external=False):
		'''Enabling/Disabling SHORTCUT in INI, plus reloading VIEW afterwards.
			shortcut_id  = STRING  ← Representing the SHORTCUT to be edited - default NEW SHORTCUT:  EMPTY STRING
			external    = BOOL    ← Optional, set TRUE to invert INI VALUE of specified 'shortcut_id' - default:  FALSE
		'''
		# If NOT external, checking if ticked - if is, disabling in INI, else enabling - then saving and reloading VIEW.
		if not external:
			disable = self.getE(f"#-shortcut-{shortcut_id}")["src"].split("/")[-1] == "tick_on.png"
		else:
			disable = self.config.get(shortcut_id, self.keys["disabled"]) == "1"

		# Removing or inverting.
		if shortcut_id[0] == self.delim and not disable:
			self.config.remove(shortcut_id)
		else:
			self.config.set(shortcut_id, self.keys["disabled"], "1" if disable else "0")

		# Saving to INI and reloading SHORTCUT's VIEW.
		self.config.save()
		self.call_DisplayConfigShortcuts()
		self.getModule("ryzom_installer").setup()

	def call_ReloadModule(self, display=""):
		'''Force complete MODULE reload.
			display  = STRING  ← Optional, accepting "profiles" or "servers" to select starting VIEW - default:  SHORTCUT's VIEW
		'''
		self.reloadModule(display)

	def call_RemoveProfile(self, args):
		'''Remove PROFILE and reload PROFILE's VIEW.
			args  = LIST/TUPLE  ← Consisting of TWO STRING(s):  PROFILE_ID & "1" if USER comply.
		'''
		profile_id, confirm = args
		confirm = confirm == "1"
		self.doRemove(profile_id, confirm)
		self.call_DisplayConfigProfiles(force=confirm)
		self.getModule("ryzom_installer").setup()

	def call_RemoveShortcut(self, args):
		'''Remove SHORTCUT and reload SHORTCUT's VIEW.
			args  = LIST/TUPLE  ← Consisting of TWO STRING(s):  SHORTCUT_ID & "1" if USER comply.
		'''
		shortcut_id, confirm = args
		confirm = confirm == "1"
		self.doRemove(shortcut_id, confirm)
		self.call_DisplayConfigShortcuts()
		self.getModule("ryzom_installer").setup()

	def call_SelectRyzomPath(self, shard):
		'''Display a BROWSE DIALOGUE and evaluating VALID PATH, invalid set DEFAULT PATH, and updating PATH FIELD.
			shard  = STRING  ← Representing the SHARD_NAME to handle.
		'''
		# Query and evaluate PATH.
		result = self.openFolderDialog()
		if result and len(result) > 0:
			selected_path = result[0]
		else:
			selected_path = self.getE(f"#-{shard}-path")["value"]
			if not selected_path:
				# Setting static DEFAULT PATH without fallback, as there's no guarantee it exists.
				selected_path = self.ryzom_user_path

		# Prepare and present PATH along with VERSION INFO if exist.
		self.setupE(f"#-{shard}-path", "value", "{}".format(self.esc(selected_path).replace("\"", "\\\"")))
		versions = self.getDomainVersionsFromPath(selected_path)
		self.updatePathInformations(shard, selected_path, versions)

	def call_ValidateRyzomPaths(self):
		'''Validating PATH(s) and creating DIRECTORY structure. Writing to INI if a path was changed. Finally loading PLAY view and refreshing installer.
		'''
		# Checking PATH of each SHARD and compare OLD to NEW.
		for shard in self.shards:
			new_path = self.getE(f"#-{shard.lower()}-path")

			if new_path and "value" in new_path:
				new_path = new_path["value"]

				# Always ensuring DIRECTORIES are created.
				if not p.isdir(new_path):
					os.makedirs(new_path)

				# Only updating INI if the PATH was indeed changed.
				self.setConfig(shard, {self.keys["location"] : new_path})

		# Refreshing installer
		self.getModule("ryzom_installer").setup()

	def call_ValidateRyzomProfiles(self):
		'''Validating PROFILE ENTRY, making sure necessary parameters are set and saving to INI if anything changed, then reload.
		'''
		for profile_id, profile in self.selected_profiles.items():
			## NOTE:  Forbidden char causes non-handling yet write-out? Why?
			if profile["name"][0] != "/" or not profile["disabled"]:
				# Assigning writable BOOL.
				profile[self.keys["disabled"]] = "1" if profile[self.keys["disabled"]] else "0"
				keys =  {
					"id"                  : profile["id"],
					self.keys["name"]     : profile[self.keys["name"]],
					self.keys["disabled"] : profile[self.keys["disabled"]],
					self.keys["shard"]    : profile[self.keys["shard"]],
					self.keys["args"]     : profile[self.keys["args"]]
				}

				# Evaluating if any value changed, if was - updating and flagging for write-out.
				self.setConfig(profile_id, keys)

		# Reloading.
		self.setup()
		self.getModule("ryzom_installer").setup()

	def call_ValidateEditProfile(self, profile_id, external_id=False):
		'''Verifying NEW or EDITED PROFILE is VALID and updating INI & CFG accordingly.
			profile_id   = STRING  ← Representing the PROFILE to be EDITED, pass EMPTY STRING if to create NEW PROFILE.
			external_id  = STRING  ← Representing the MODULE, or PREFIX, used to identify FIELD(s) when called from external view.
		'''
		def checkFolder(name):
			'''Evaluating if DIRECTORY NAME is VALID and not already in use.
				name  = STRING  ← Representing DIRECTORY NAME to be evaluated.
			Returning BOOL - TRUE if VALID, else FALSE.
			'''
			# Returning FALSE if no NAME supplied.
			if not name:
				return False

			# Returning FALSE if invalid characters detected in NAME.
			for c in name:
				oc = ord(c)
				if not ((oc >= ord("a") and oc <= ord("z")) or (oc >= ord("A") and oc <= ord("Z")) or (oc >= ord("0") and oc <= ord("9")) or c == "_"):
					return False

			# Returning FALSE if DIRECTORY exist.
			if self.k.isdir(self.getLocationUser(), name):
				return False

			# ...otherwise assuming NAME is VALID and returning TRUE.
			return True

		# If called from external module, using it as prefix instead of SELF for fetching DATA from FIELD(s).
		id = external_id if external_id and isinstance(external_id, str) and len(external_id) else "#"

		# Fetch data from FIELD(s), issue warning and skip if invalid:  PROFILE_NAME
		name = self.getE(f"{id}-edit-name", "value")
		shard = self.getE(f"{id}-edit-shard", "value")
		if not name:
			invalid = self._("invalid_profile_name")
			if not external_id:
				self.setMessage(invalid, warning=True)
			else:
				return invalid
		else:
			# Evaluate NEW PROFILE.
			if not profile_id:
				# Evaluate DIRECTORY, issue warning and skip if invalid:  DIRECTORY
				folder = self.getE(f"{id}-edit-folder", "value")
				if not checkFolder(folder):
					invalid = self._('invalid_profile_folder')
					if not external_id:
						self.setMessage(invalid, warning=True)
						self.hideE("popup")
						return
					else:
						return invalid

				# If PROFILE(s) are listed, make sure NEW PROFILE doesn't overwrite existing ENTRIES.
				i = len(self.selected_profiles)-1
				profile_id = f"profile_{i}"
				while profile_id in self.selected_profiles:
					i += 1
					profile_id = f"profile_{i}"

				# Saving to INI if anything has changed.
				self.setConfig(profile_id, {self.keys["folder"] : folder})

			# Make sure to load USER and ROOT CFG if exist, and create DIRECTORY structure first if doesn't exist.
			folder = self.config.get(profile_id, self.keys["folder"])
			cfgname = self.getLocationFileUserCfg(folder)
			if not self.k.isfile(cfgname):
				self.createProfileFolder(folder)
			self.updateShardInProfile(shard, folder)

			# Set value, if any has changed - if so, flag for write-out.
			keys = {
				self.keys["name"]  : name,
				self.keys["shard"] : shard
			}

			# Saving to INI if anything changed, updating LANGUAGE and reload PROFILE's VIEW.
			self.setConfig(profile_id, keys)
			self.updateLangInProfile(shard, self.lang, folder)
			self.call_DisplayConfigProfiles(force=True)
			self.getModule("ryzom_installer").setup()

		# Hiding popup DIALOGUE.
		if not external_id:
			self.hideE("popup")

	def call_ValidateEditShortcut(self, shortcut_id, external_id=False):
		'''Verifying NEW or EDITED SHORTCUT is VALID and updating INI accordingly.
			shortcut_id  = STRING  ← Representing the SHORTCUT to be EDITED, pass EMPTY STRING if to create NEW SHORTCUT.
			external_id  = STRING  ← Representing the MODULE, or PREFIX, used to identify FIELD(s) when called from external view.
		'''
		# If called from external module, using it as prefix instead of SELF for fetching DATA from FIELD(s).
		id = external_id if external_id and isinstance(external_id, str) and len(external_id) else "#"

		# Fetch data from FIELD(s), issue warning and skip if invalid:  SHORTCUT_NAME
		name = self.getE(f"{id}-edit-name", "value")
		if not name:
			invalid = self._("invalid_shortcut_name")
			if not external_id:
				self.setMessage(invalid, warning=True)
			else:
				return invalid
		else:
			# Fetch data from FIELD(s) and validate.
			profile = self.getE(f"{id}-edit-profile", "value")
			slot = self.getE(f"{id}-edit-argslot", "value")
			if slot:
				args = [self.getE(f"{id}-edit-argname", "value"), self.getE(f"{id}-edit-argpassword", "value"), "slot", slot]
			else:
				args = [self.getE(f"{id}-edit-argname", "value"), self.getE(f"{id}-edit-argpassword", "value")]
			args = " ".join(args).replace("  ", " ")

			# Evaluate NEW SHORTCUT.
			if not shortcut_id:
				# If SHORTCUT(s) are listed, make sure NEW SHORTCUT doesn't overwrite existing ENTRIES.
				i = len(self.selected_shortcuts)-1
				shortcut_id = self.delim.join([self.sKey, str(i)])
				while shortcut_id in self.selected_shortcuts:
					i += 1
					shortcut_id = self.delim.join([self.sKey, str(i)])

			# Set value, if any has changed - if so, flag for write-out.
			keys =  {
				self.keys["name"]     : name,
				self.keys["profile"]  : profile,
				self.keys["args"]     : args,
				self.keys["disabled"] : "0"
			}

			# Saving to INI if anything changed and reload SHORTCUT's VIEW.
			if not shortcut_id.startswith(self.prefix[self.sKey]):
				shortcut_id = self.delim.join([self.sKey, name])
			self.setConfig(shortcut_id, keys)
			self.call_DisplayConfigShortcuts()
			self.getModule("ryzom_installer").setup()

		# Hiding popup DIALOGUE.
		if not external_id:
			self.hideE("popup")



	def setup(self, display=""):
		'''Preparing module for use.
			display  = STRING  ← Optional, accepting "profiles" or "servers" to select starting VIEW - default:  SHORTCUT's VIEW
		'''
		## NOTE:  Handle checking if module is to be reloaded or resumed where-was.
		if self.preventReloadModule():
			self.warning(f'[RESUMING]  Not reloading:  {self.id}')
			return
		else:
			self.log(f'[INITIALIZING]  Loading sequence of:  {self.id}')

		# Load saved SHARD(s) and PROFILE(s) & SHORTCUT(s) from INI  --  Importing from old installer's INI if necessary.
		self.log("Getting servers...")
		self.getServersFromIni()

		if not self.config.fileExists:
			self.importProfilesAndShortcuts()

		self.log("Getting profiles...")
		self.getProfilesFromIni()

		self.log("Getting shortcuts...")
		self.getShortcutsFromIni()

		# Selecting VIEW to display.
		if display == "servers":
			self.call_DisplayConfigServers()
		elif display == "profiles":
			self.call_DisplayConfigProfiles()
		else:
			self.call_DisplayConfigShortcuts()

		# Loading global JS.
		self.evalFile("edit_shortcut.js")

		# Declaring done setting things up.
		KyssModule.setup(self)
		self.log(f'[FINILIZED]  Loading sequence of:  {self.id}')

		# Allow resuming at current tab instead of reloading entire module for no reason.
		self.module_prevent_reload = True
